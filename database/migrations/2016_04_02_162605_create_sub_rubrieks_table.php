<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubRubrieksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_rubrieks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rubrieks_nummer');
            $table->integer('sub_rubrieks_nummer');
            $table->string('naam',512);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_rubrieks');
    }
}
