<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoekingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boekings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('factuur_id');
            $table->integer('grootboekrekening_id');
            $table->text('beschrijving');
            $table->decimal('totaal', 11, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('boekings');
    }
}
