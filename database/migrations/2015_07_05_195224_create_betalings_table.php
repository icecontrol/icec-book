<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetalingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('betalings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('factuur_id');
            $table->integer('grootboekrekening_id');            
            $table->string('betaaldatum');
            $table->string('totaal');
            $table->integer('valuta_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('betalings');
    }
}
