<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrootboekrekeningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grootboekrekenings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sub_rubrieks_nummer');
            $table->integer('grootboekrekening_nummer');
            $table->string('naam',512);
            $table->boolean('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grootboekrekenings');
    }
}
