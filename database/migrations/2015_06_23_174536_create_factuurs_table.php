<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactuursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factuurs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('factuurref');
            $table->string('factuurdatum');
            $table->integer('relatie_id');
            $table->integer('valuta_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('factuurs');
    }
}
