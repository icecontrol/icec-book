<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelatiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relaties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bedrijf');
            $table->string('btw');
            $table->string('bank');
            $table->string('telefoon');
            $table->string('email');
            $table->string('website');
            $table->string('contact');
            $table->string('straat');
            $table->string('postcode');
            $table->string('plaats');
            $table->string('land');
            $table->string('afstand');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('relaties');
    }
}
