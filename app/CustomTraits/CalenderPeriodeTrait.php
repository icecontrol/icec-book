<?php
namespace App\CustomTraits;
/**
 * Created by I.C.E.C
 * Date: 19-6-16
 * Time: 23:26
 */
/**
 * Class CalenderPeriodeTrait
 *
 * @package App\CustomTraits
 */
trait CalenderPeriodeTrait {
    
    /**
     * @param $jaar
     * @param $periode
     * @return array
     */
    public static function GetCalenderPeriodes($jaar, $periode) {
        $kwartaalstart = '';
        $kwartaaleinde = '';
        
        switch ($periode) {
            case 1:
                $kwartaalstart = '-01-01';
                $kwartaaleinde = '-03-31';
                break;
            case 2:
                $kwartaalstart = '-04-01';
                $kwartaaleinde = '-06-30';
                break;
            case 3:
                $kwartaalstart = '-07-01';
                $kwartaaleinde = '-09-30';
                break;
            case 4:
                $kwartaalstart = '-10-01';
                $kwartaaleinde = '-12-31';
                break;
            case 5:
                $kwartaalstart = '-01-01';
                $kwartaaleinde = '-12-31';
                break;
        }
    
        return array(
            'periodestart' => $jaar . '' . $kwartaalstart, 
            'periodeeinde' => $jaar . '' . $kwartaaleinde
        );
    }

    public static function GetHuidigeKwartaalPeriode() {
        return array(
            'periodestart' => self::getBeginVanHuidigeKwartaal(),
            'periodeeinde' => self::getEindeVanHuidgeKwartaal()
        );
    }

    /**
     * @return bool|string
     */
    public static function getBeginVanHuidigeKwartaal()
    {
        $current_quarter = ceil(date('n') / 3);
        return  $first_date = date('Y-m-d',strtotime(date('Y').'-'.(($current_quarter*3)-2).'-1'));        
    }

    /**
     * @return bool|string
     */
    public static function getEindeVanHuidgeKwartaal()
    {
        $current_quarter = ceil(date('n') / 3);
        return date('Y-m-t',strtotime(date('Y').'-'.(($current_quarter*3)).'-1'));
    }
    
    /**
     * @return bool|string
     */
    public static function getBeginKwartaalVanDatum($datum)
    {        
        $current_quarter = ceil($datum->format('m') / 3);
        return  $first_date = date('Y-m-d',strtotime($datum->format('y').'-'.(($current_quarter*3)-2).'-1'));        
    }

    /**
     * @return bool|string
     */
    public static function getEindeKwartaalVanDatum($datum)
    {
        $current_quarter = ceil($datum->format('m') / 3);
        return date('Y-m-t',strtotime($datum->format('y').'-'.(($current_quarter*3)).'-1'));
    }

    
}