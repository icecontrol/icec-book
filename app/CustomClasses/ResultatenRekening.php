<?php
namespace App\CustomClasses;

use App\Boeking;
use App\CustomTraits\CalenderPeriodeTrait;
use App\Grootboekrekening;
use App\Rubriek;
use App\SubRubriek;


/**
 * Class ResultatenRekening
 *
 * @package App\CustomClasses
 */
class ResultatenRekening {
    
    /**
     * @var
     */
    protected $EUR;

    /**
     * @var
     */
    private $overzicht;
    
    /**
     * @var
     */
    private $valutas;
    
    /**
     * @var
     */
    private $valutatotals;
    
    /**
     * @param $jaar
     * @param $periode
     * @return array
     */
    public function BuidResultatenRekening($jaar, $periode, $type) {
        $periodes = CalenderPeriodeTrait::GetCalenderPeriodes($jaar, $periode);
        foreach (Rubriek::whereType($type)->orderby('rubrieks_nummer', 'ASC')->get() as $rubriek) {
            foreach (SubRubriek::whereRubrieksId($rubriek->id)->orderby('sub_rubrieks_nummer', 'ASC')->get() as $subrubriek) {
                foreach (Grootboekrekening::whereSubRubrieksId($subrubriek->id)->orderby('grootboekrekening_nummer',
                    'ASC')->get() as $grootboekrekening) {
                    foreach (Boeking::whereGrootboekrekeningId($grootboekrekening->id)->get() as $boeking) {
                        if (true == $this->dateIsInBetween(new \DateTime($periodes['periodestart']),
                                new \DateTime($periodes['periodeeinde']),
                                new \DateTime($boeking->factuur->factuurdatum))
                        ) {
                            $this->addToOverzicht($rubriek, $subrubriek, $grootboekrekening, $boeking);
                        }
                    }
                }
            }
        }
        return $this->getOverzicht();
    }
    
    /**
     *  Checks if $subject dat in in between to and from dates , or on the given date
     * @param \DateTime $from
     * @param \DateTime $to
     * @param \DateTime $subject
     * @return bool
     */
    private function dateIsInBetween(\DateTime $from, \DateTime $to, \DateTime $subject) {
        return $subject->getTimestamp() >= $from->getTimestamp() && $subject->getTimestamp() <= $to->getTimestamp() ? true : false;
    }
    
    /**
     * @param Rubriek $rubriek
     * @param SubRubriek $subrubriek
     * @param Grootboekrekening $grootboekrekening
     * @param Boeking $boeking
     */
    private function addToOverzicht(Rubriek $rubriek, SubRubriek $subrubriek, Grootboekrekening $grootboekrekening, Boeking $boeking) {
        $this->overzicht
        [$rubriek->rubrieks_nummer . ' | ' . $rubriek->naam]
        [$rubriek->rubrieks_nummer . $subrubriek->sub_rubrieks_nummer . ' | ' . $subrubriek->naam]
        [$rubriek->rubrieks_nummer
        . $subrubriek->sub_rubrieks_nummer
        . $grootboekrekening->grootboekrekening_nummer
        . ' | ' . $grootboekrekening->naam][] = $boeking;
    }
    
    /**
     * @return array
     */
    public function getOverzicht() {
        return $this->overzicht;
    }
    
    public function Resultaten($periode_start,$periode_einde, $type) {
        foreach (Rubriek::whereType($type)->orderby('rubrieks_nummer', 'ASC')->get() as $rubriek) {
            foreach (SubRubriek::whereRubrieksId($rubriek->id)->orderby('sub_rubrieks_nummer', 'ASC')->get() as $subrubriek) {
                foreach (Grootboekrekening::whereSubRubrieksId($subrubriek->id)->orderby('grootboekrekening_nummer',
                    'ASC')->get() as $grootboekrekening) {
                    $boekingen = Boeking::whereGrootboekrekeningId($grootboekrekening->id)->get();
                    foreach ($boekingen as $boeking) {
                        if (true == $this->dateIsInBetween(new \DateTime($periode_start),
                                new \DateTime($periode_einde),
                                new \DateTime($boeking->factuur->factuurdatum))
                        ) {
                            $this->addToTotaalsList($rubriek, $subrubriek, $grootboekrekening, $boeking);
                        }
                    }
                    $this->getTotalBedragen();
                }
            }
        }
        $this->WinstVerliesTotaal();
        
        return array(
            'valutatotals' => $this->getValutatotals(),
            'EUR' => $this->getEUR(),
        );
    }
    
    /**
     * @param Rubriek $rubriek
     * @param SubRubriek $subrubriek
     * @param Grootboekrekening $grootboekrekening
     * @param Boeking $boeking
     */
    private function addToTotaalsList(Rubriek $rubriek, SubRubriek $subrubriek, Grootboekrekening $grootboekrekening, Boeking $boeking) {

        if($boeking->factuur->Valuta->id == 1 ){
            $totaal = $boeking->totaal;
        }else{
            $totaal= $boeking->factuur->betaling()->where('valuta_id', 1)->get()->sum('totaal');
            if($totaal == null){
                $totaal = (new CurrencyConverter())->currency($boeking->factuur->Valuta->naam, 'EUR', $boeking->totaal );
            }
        }
        
        $this->valutas
        [$rubriek->rubrieks_nummer
        . $subrubriek->sub_rubrieks_nummer
        . $grootboekrekening->grootboekrekening_nummer
        . ' | ' . $grootboekrekening->naam][$grootboekrekening->type]['EUR'][] = $totaal;
    }

    private function getTotalBedragen() {
        if (count($this->valutas) > 0) {
            foreach ($this->valutas as $grootboek => $boekings) {
                foreach ($boekings as $inkomst_uitgave => $valutas) {
                    foreach ($valutas as $val => $valutavalues) {
                        $this->valutatotals[$grootboek][$inkomst_uitgave][$val] = array_sum($valutavalues);
                    }
                }
            }
        }
    }
    
    private function WinstVerliesTotaal() {
        if ((count($this->valutatotals)) > 0) {
            foreach ($this->valutatotals as $ValutaSums) {
                foreach ($ValutaSums as $inkomst_uitgave => $Sums) {
                    foreach ($Sums as $valuta => $bedrag) {
                        if ($inkomst_uitgave == 0) {
                            $this->$valuta -= $bedrag;
                        } else {
                            $this->$valuta += $bedrag;
                        }
                    }
                }
            }
        }
    }
    
    /**
     * @return mixed
     */
    public function getValutatotals() {
        return $this->valutatotals;
    }
    
    /**
     * @return mixed
     */
    public function getEUR() {
        return $this->EUR;
    }
    
    /**
     * @return mixed
     */
    public function getValutas() {
        return $this->valutas;
    }
}