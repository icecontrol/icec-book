<?php
/**
 * Created by I.C.E.C
 * Date: 15-7-16
 * Time: 16:42
 */

namespace App\CustomClasses;

use DOMDocument;
use DOMXPath;

class CurrencyConverter {
    
    function currency($from, $to, $amount)
    {
        $content = file_get_contents('https://www.google.com/finance/converter?a='.$amount.'&from='.$from.'&to='.$to);        
        $doc = new DOMDocument;
        @$doc->loadHTML($content);
        $xpath = new DOMXpath($doc);        
        $result = $xpath->query('//*[@id="currency_converter_result"]/span')->item(0)->nodeValue;        
        return str_replace(' '.$to, '', $result);
    }
    
}