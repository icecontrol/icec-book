<?php
namespace App\CustomClasses;

use App\Factuur;
use App\Grootboekrekening;

/**
 * Class CrediteurenService
 *
 * @package App\CustomClasses
 */
class BetalingenService {
    
    /**
     * @return array
     */
    public function CrediteurenList() {
        $return = array();
        foreach (Factuur::all() as $factuur) {
            if (count($factuur->boekings) > 0) {
                if ($this->IsUitgave($factuur)) {
                    if ($this->HeeftOpenstaandBedrag($factuur)) {
                        $return[] = $factuur;
                    }
                }
            }
        }
        return $return;
    }
    
    /**
     * @param $factuur
     * @return bool
     */
    private function IsUitgave(Factuur $factuur) {
        foreach ($factuur->boekings as $boeking) {
            if ($boeking->grootboekrekening->type == 0) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @param Factuur $factuur
     * @return bool
     */
    private function HeeftOpenstaandBedrag(Factuur $factuur) {
        if (number_format($factuur->betaling()->where('valuta_id', $factuur->Valuta->id)->get()->sum('totaal'),
                2) < number_format($factuur->boekings()->where('grootboekrekening_id','!=', 34)->get()->sum('totaal'), 2)
        ) {
            return true;
        }
        return false;
    }
    
    /**
     * @return array
     */
    public function DebiteurenList() {
        $return = array();
        foreach (Factuur::all() as $factuur) {
            if (count($factuur->boekings) > 0) {
                if ($this->IsInkomst($factuur)) {
                    if ($this->HeeftOpenstaandBedrag($factuur)) {
                        $return[] = $factuur;
                    }
                }
            }
        }
        return $return;
    }
    
    /**
     * @param $factuur
     * @return bool
     */
    private function IsInkomst(Factuur $factuur) {
        foreach ($factuur->boekings as $boeking) {
            if ($boeking->grootboekrekening->type == 1) {
                return true;
            }
        }
        return false;
    }
    

}