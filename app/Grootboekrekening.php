<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Grootboekrekening
 *
 * @package App
 */
class Grootboekrekening extends Model {
    /**
     * @var string
     */
    protected $table = 'grootboekrekenings';
    
    /**
     * @var array
     */
    protected $fillable = [
        'sub_rubrieks_id',
        'grootboekrekening_nummer',
        'naam',
        'type',
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subrubriek() {
        return $this->belongsTo('App\SubRubriek','sub_rubrieks_id','id');
    }

    
    public function getFullGrootBoekNummer()
    {
        return $this->subrubriek->rubriek->rubrieks_nummer. $this->subrubriek->sub_rubrieks_nummer . $this->grootboekrekening_nummer;
    }
    
    public function boekings(){
        return $this->hasMany('App\Boeking');
    }
}
