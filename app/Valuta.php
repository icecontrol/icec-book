<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Valuta extends Model
{
    protected $table = 'valutas';

    protected $fillable = [
        'naam',
        'symbool',
    ];
}
