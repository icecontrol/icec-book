<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boeking extends Model
{
    
    protected $table = 'boekings';
    
    /**
     * @var array
     */
    protected $fillable = [
        'factuur_id',
        'grootboekrekening_id',
        'beschrijving',
        'totaal',
    ];
    
    public function factuur()
    {
        return $this->belongsTo('App\Factuur');
    }
    
    public function grootboekrekening()
    {
        return $this->belongsTo('App\Grootboekrekening');
    }
}
