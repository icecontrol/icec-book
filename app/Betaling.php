<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Betaling extends Model
{
    //
    protected $table = 'betalings';


    public function factuur()
    {
        return $this->belongsTo('App\Factuur');
    }
    public function Grootboekrekening()
    {
        return $this->belongsTo('App\Grootboekrekening');
    }
    
    public function Valuta() {
        return $this->belongsTo('App\Valuta');
        
    }
}
