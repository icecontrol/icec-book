<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//

Route::get('/about'                                         , 'Auth\AuthController@getLogin');
Route::post('/about'                                         , 'Auth\AuthController@postLogin');

Route::get('/overzichten/{type}/{jaar}/{periode}'                    , 'Overzichten\ResultatenrekeningController@ShowResultatenrekening');

Route::controllers([
    '/instelling/valuta' 	=> 'Instelling\ValutaController',
    '/instelling/rubriek' 	=> 'Instelling\RubriekController',
    '/instelling/subrubriek' 	=> 'Instelling\SubRubriekController',
    '/instelling/grootboekrekeningen' 	=> 'Instelling\GrootboekrekeningController',

    '/invoer/betaling/Crediteuren' 	=> 'Invoer\Betaling\CrediteurenController',
    '/invoer/betaling/Debiteuren' 	=> 'Invoer\Betaling\DebiteurenController',
    '/invoer/betaling' 	=> 'Invoer\Betaling\BetalingController',
    '/invoer/relatie' 	=> 'Invoer\Relatie\RelatieController',
    '/instelling' 	=> 'Instelling\InstellingController',

    '/invoer/factuur' 	=> 'Invoer\Factuur\FactuurController',
    '/overzichten' 	=> 'OverzichtenController',
//    '/overzichten/resultaten' 	=> 'Overzichten\ResultatenrekeningController',

    '/api' 	=> 'ApiController',
    '/' 	=> 'MainController',
]);

Route::get('/product'                                       , 'ProductController@index');
Route::get('/product/create'                                , 'ProductController@create');

