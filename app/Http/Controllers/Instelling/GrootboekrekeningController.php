<?php

namespace App\Http\Controllers\Instelling;

use App\Grootboekrekening;
use App\Http\Controllers\Controller;
use App\SubRubriek;
use Illuminate\Http\Request;

use App\Http\Requests;

/**
 * Class GrootboekrekeningController
 *
 * @package App\Http\Controllers
 */
class GrootboekrekeningController extends Controller {
    
    /**
     * GrootboekrekeningController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->middleware('logedin');
    }
    
    /**
     * @return mixed
     */
    public function getIndex() {
        $grootboekrekeningen = Grootboekrekening::all();
        return view('instellingen.grootboekrekeningen.index', compact('grootboekrekeningen'));
    }
    
    /**
     * @return mixed
     */
    public function getAdd() {
        $subrubrieks = array();
        foreach (SubRubriek::orderBy('sub_rubrieks_nummer', 'ASC')->get() as $subrubriek) {
            $subrubrieks['Rubriek ' . $subrubriek->rubriek->rubrieks_nummer . ' | ' . $subrubriek->rubriek->naam][$subrubriek->id] = 'Subrubriek ' . $subrubriek->getFullSubrubriekNummer() . ' | ' . $subrubriek->naam;
        }        
        return view('instellingen.grootboekrekeningen.add', compact('subrubrieks'));
    }
    
    /**
     * @param Request $request
     * @return mixed
     */
    public function postAdd(Request $request) {
        
        $grootboekrekening = new Grootboekrekening();
        $grootboekrekening->sub_rubrieks_id = $request->sub_rubrieks_id ;
        $grootboekrekening->grootboekrekening_nummer = $request->grootboekrekening_nummer ;
        $grootboekrekening->naam = $request->naam ;
        $grootboekrekening->type = $request->type ;
        $grootboekrekening->save();

//        Grootboekrekening::create($request->all());
        return redirect()->intended('/instelling/grootboekrekeningen');
    }
    
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id) {
        $subrubrieks = array();
        foreach (SubRubriek::orderBy('sub_rubrieks_nummer', 'ASC')->get() as $subrubriek) {
            $subrubrieks['Rubriek ' . $subrubriek->rubriek->rubrieks_nummer . ' | ' . $subrubriek->rubriek->naam][$subrubriek->id] = 'Subrubriek ' . $subrubriek->getFullSubrubriekNummer() . ' | ' . $subrubriek->naam;
        }        
        $grootboekrekening = Grootboekrekening::find($id);
        return view('instellingen.grootboekrekeningen.edit', compact('subrubrieks', 'grootboekrekening'));
    }
    
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(Request $request, $id) {
        Grootboekrekening::find($id)->update($request->all());
        return redirect()->intended('/instelling/grootboekrekeningen');
    }
}
