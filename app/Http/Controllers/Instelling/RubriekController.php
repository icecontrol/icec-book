<?php

namespace App\Http\Controllers\Instelling;

use App\Http\Controllers\Controller;
use App\Rubriek;
use Illuminate\Http\Request;

use App\Http\Requests;

/**
 * Class RubriekController
 *
 * @package App\Http\Controllers
 */
class RubriekController extends Controller {
    
    /**
     * RubriekController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->middleware('logedin');
    }
        
    /**
     * @return mixed
     */
    public function getIndex() {
        
        $rubrieks = Rubriek::all();
        return view('instellingen.rubriek.index',compact('rubrieks'));
    }
    
    /**
     * @param $id
     * @return mixed
     */
    public function getEdit($id) {
        $rubriek = Rubriek::find($id);
        return view('instellingen.rubriek.edit',compact('rubriek'));
    }
    
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function postEdit(Request $request, $id) {
        Rubriek::find($id)->update($request->all());
        return redirect()->intended('/instelling/rubriek');
    }
}
