<?php

namespace App\Http\Controllers\Instelling;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

/**
 * Class InstellingController
 *
 * @package App\Http\Controllers
 */
class InstellingController extends Controller {
    
    /**
     * InstellingController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->middleware('logedin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex() {
        return view('instellingen.index');
    }
}
