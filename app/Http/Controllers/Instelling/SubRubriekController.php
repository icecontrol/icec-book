<?php

namespace App\Http\Controllers\Instelling;

use App\Http\Controllers\Controller;
use App\Rubriek;
use App\SubRubriek;
use Illuminate\Http\Request;

use App\Http\Requests;

/**
 * Class SubRubriekController
 *
 * @package App\Http\Controllers
 */
class SubRubriekController extends Controller {
    
    /**
     * SubRubriekController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->middleware('logedin');
    }
        
    /**
     * @return mixed
     */
    public function getIndex() {
        
        $subrubrieks = SubRubriek::all();
        return view('instellingen.subrubriek.index', compact('subrubrieks'));
    }
    
    /**
     * @return mixed
     */
    public function getAdd() {        
        $rubrieks = array();
        foreach (Rubriek::all() as $rubriek) {
            $rubrieks[$rubriek->id] = 'Rubriek ' . $rubriek->rubrieks_nummer . ' | ' . $rubriek->naam;
        }        
        return view('instellingen.subrubriek.add', compact('rubrieks'));
    }
    
    /**
     * @param Request $request
     * @return mixed
     */
    public function postAdd(Request $request) {
        SubRubriek::create($request->all());
        return redirect()->intended('/instelling/subrubriek');
    }
    
    /**
     * @param $id
     * @return mixed
     */
    public function getEdit($id) {
        $rubrieks = array();
        foreach (Rubriek::all() as $rubriek) {
            $rubrieks[$rubriek->rubrieks_nummer] = 'Rubriek ' . $rubriek->rubrieks_nummer . ' | ' . $rubriek->naam;
        }
        $subrubriek = SubRubriek::find($id);
        return view('instellingen.subrubriek.edit', compact('subrubriek', 'rubrieks'));
    }
    
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function postEdit(Request $request, $id) {
        SubRubriek::find($id)->update($request->all());
        return redirect()->intended('/instelling/subrubriek');
    }
}
