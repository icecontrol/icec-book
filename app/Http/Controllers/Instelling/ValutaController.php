<?php

namespace App\Http\Controllers\Instelling;

use App\Http\Controllers\Controller;
use App\Valuta;
use Illuminate\Http\Request;
use App\Http\Requests;

/**
 * Class ValutaController
 *
 * @package App\Http\Controllers
 */
class ValutaController extends Controller {
        
    /**
     * SubRubriekController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->middleware('logedin');
    }
    
    /**
     * @return mixed
     */
    public function getIndex() {
        
        $valutas = Valuta::all();
        return view('instellingen.valuta.index', compact('valutas'));
    }
    
    /**
     * @return mixed
     */
    public function getAdd() {
        return view('instellingen.valuta.add');
    }
    
    /**
     * @param Request $request
     * @return mixed
     */
    public function postAdd(Request $request) {
        Valuta::create($request->all());
        return redirect()->intended('/instelling/valuta');
    }
    
    /**
     * @param $id
     * @return mixed
     */
    public function getEdit($id) {
        $valuta = Valuta::find($id);
        return view('instellingen.valuta.edit', compact('valuta'));
    }
    
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function postEdit(Request $request, $id) {
        Valuta::find($id)->update($request->all());
        return redirect()->intended('/instelling/valuta');
    }
}
