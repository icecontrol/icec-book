<?php
namespace App\Http\Controllers;

use App\CustomClasses\ResultatenRekening;
use App\CustomTraits\CalenderPeriodeTrait;
use App\Factuur;
use DateTime;
use Illuminate\Http\Request;

use App\Http\Requests;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 */
class ApiController extends Controller {

    /**
     * @var
     */
    protected $EersteFactuurDatum;

    /**
     * ApiController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->middleware('logedin');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHuidigePeriodeResultaten() {
        $resultaat = $this->getResultatenVanGegevenPeriode(CalenderPeriodeTrait::getBeginVanHuidigeKwartaal(),
            CalenderPeriodeTrait::getEindeVanHuidgeKwartaal());
        return response()->json(array(
            'inkomstenarray' => $resultaat['inkomstenarray'],
            'uitgavesarray' => $resultaat['uitgavesarray'],
        ));
    }

    /**
     * @param $begin_datum
     * @param $eind_datum
     * @return array
     */
    private function getResultatenVanGegevenPeriode($begin_datum, $eind_datum) {
        $uitgave_array = array();
        $inkomsten_array = array();

        $resultaten = (new ResultatenRekening())->Resultaten($begin_datum, $eind_datum, 'Resultaat');

        foreach ($resultaten['valutatotals'] as $key => $grootboek) {
            if (array_key_exists(0, $grootboek)) {
                $uitgave_array[] = array(
                    'grootboek' => $key,
                    'totaal' => $grootboek[0]['EUR'],
                );
            }
            if (array_key_exists(1, $grootboek)) {
                $inkomsten_array[] = array(
                    'grootboek' => $key,
                    'totaal' => $grootboek[1]['EUR'],
                );
            }
        }
        return array(
            'inkomstenarray' => $inkomsten_array,
            'uitgavesarray' => $uitgave_array,
        );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWinstVerliesResultaten() {

        $winst_verlies_array = array();

        foreach ($this->getWinstVerliestKwartaalPeriodes() as $periode_resultaat) {

            $resultaat = $this->getResultatenVanGegevenPeriode($periode_resultaat['periode-start'], $periode_resultaat['einde-periode']);

            $uitgaves = array_sum(array_column($resultaat['uitgavesarray'], 'totaal'));
            $inkomsten = array_sum(array_column($resultaat['inkomstenarray'], 'totaal'));
            $winstverlies = $inkomsten - $uitgaves;

            $winst_verlies_array[] = array(
                "periode" => $periode_resultaat['periode'],
                "winstverlies" => $winstverlies,
                "inkomsten" => $inkomsten,
                "uitgaves" => $uitgaves,
            );
        }

        return response()->json(array(
            'winst_verlies_array' => $winst_verlies_array,
        ));
    }

    /**
     * @return mixed
     */
    private function getWinstVerliestKwartaalPeriodes() {
        $this->setDatumEersteFactuur();
        $boekhoudinglifetime = $this->addKwartaalsVanHuidigeJaar(
            $this->addKwartalenTussenliggendeJaren(
                $this->addKwartaalsEersteJaarVanaf(array()
                )
            )
        );
        return $boekhoudinglifetime;
    }

    /**
     *
     */
    private function setDatumEersteFactuur() {
        $this->EersteFactuurDatum = new DateTime(Factuur::orderBy('factuurdatum', 'ASC')->first()->factuurdatum);
        return;
    }

    /**
     * @param $boekhoudinglifetime
     * @return mixed
     */
    private function addKwartaalsVanHuidigeJaar($boekhoudinglifetime) {
        if ((new DateTime("now"))->format('m') <= 3) {
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime((new DateTime("now"))->format('Y') . '-2-1')));
            return $boekhoudinglifetime;
        };
        if ((new DateTime("now"))->format('m') <= 6) {
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime((new DateTime("now"))->format('Y') . '-2-1')));
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime((new DateTime("now"))->format('Y') . '-4-1')));
            return $boekhoudinglifetime;
        };
        if ((new DateTime("now"))->format('m') <= 9) {
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime((new DateTime("now"))->format('Y') . '-2-1')));
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime((new DateTime("now"))->format('Y') . '-4-1')));
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime((new DateTime("now"))->format('Y') . '-7-1')));
            return $boekhoudinglifetime;
        };
        array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime((new DateTime("now"))->format('Y') . '-2-1')));
        array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime((new DateTime("now"))->format('Y') . '-4-1')));
        array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime((new DateTime("now"))->format('Y') . '-7-1')));
        array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime((new DateTime("now"))->format('Y') . '-11-1')));
        return $boekhoudinglifetime;
    }

    /**
     * @param DateTime $date
     * @return array
     */
    private function KwartaalPeriode(DateTime $date) {
        return array(
            'periode' => 'Q' . $this->getKwartaal($date) . ' ' . $date->format('Y'),
            'periode-start' => CalenderPeriodeTrait::getBeginKwartaalVanDatum($date),
            'einde-periode' => CalenderPeriodeTrait::getEindeKwartaalVanDatum($date),
        );
    }

    /**
     * @param $date
     * @return int
     */
    private function getKwartaal($date) {
        $month = $date->format('m');
        if ($month <= 3)
            return 1;
        if ($month <= 6)
            return 2;
        if ($month <= 9)
            return 3;
        return 4;
    }

    /**
     * @param $boekhoudinglifetime
     * @return mixed
     */
    private function addKwartalenTussenliggendeJaren($boekhoudinglifetime) {
        for ($year = $this->getEersteFactuurDatum()->format('Y') + 1; $year < (new DateTime("now"))->format('Y'); $year++) {
            $boekhoudinglifetime = $this->addKwartaalsVanVolledigeJaar($boekhoudinglifetime, $year);
        }
        return $boekhoudinglifetime;
    }

    /**
     * @return mixed
     */
    public function getEersteFactuurDatum() {
        return $this->EersteFactuurDatum;
    }

    /**
     * @param $boekhoudinglifetime
     * @param $jaar
     * @return mixed
     */
    private function addKwartaalsVanVolledigeJaar($boekhoudinglifetime, $jaar) {
        array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($jaar . '-2-1')));
        array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($jaar . '-4-1')));
        array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($jaar . '-7-1')));
        array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($jaar . '-11-1')));
        return $boekhoudinglifetime;
    }

    /**
     * @param $boekhoudinglifetime
     * @return mixed
     */
    private function addKwartaalsEersteJaarVanaf($boekhoudinglifetime) {
        $date = $this->getEersteFactuurDatum();
        if ($date->format('m') <= 3) {
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($date->format('Y') . '-2-1')));
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($date->format('Y') . '-4-1')));
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($date->format('Y') . '-7-1')));
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($date->format('Y') . '-11-1')));
            return $boekhoudinglifetime;
        };
        if ($date->format('m') <= 6) {
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($date->format('Y') . '-4-1')));
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($date->format('Y') . '-7-1')));
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($date->format('Y') . '-11-1')));
            return $boekhoudinglifetime;
        };
        if ($date->format('m') <= 9) {
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($date->format('Y') . '-7-1')));
            array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($date->format('Y') . '-11-1')));
            return $boekhoudinglifetime;
        };
        array_push($boekhoudinglifetime, $this->KwartaalPeriode(new DateTime($date->format('Y') . '-11-1')));
        return $boekhoudinglifetime;
    }
}
