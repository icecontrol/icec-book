<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class OverzichtenController extends Controller
{
    //
    protected $soort;
    protected $id;

    public function __construct(Request $request)
    {
        $this->middleware('logedin');
        $this->soort = $request->soort;
        $this->id = $request->id;
    }

    public function getIndex()
    {
        return view('overzichten.index');
    }
    
   public function postIndex(Request $request)
    {

        return redirect()->intended('/overzichten/'.$request->type.'/'.$request->jaar.'/'. $request->periode);
    }
}
