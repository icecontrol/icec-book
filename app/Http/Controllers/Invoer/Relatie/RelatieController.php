<?php

namespace App\Http\Controllers\Invoer\Relatie;

use App\Http\Controllers\Controller;
use App\Relatie;
use Illuminate\Http\Request;

/**
 * Class RelatieController
 *
 * @package App\Http\Controllers\Invoer\Relatie
 */
class RelatieController extends Controller {
    /**
     * RelatieController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->middleware('logedin');
    }
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex() {
        $relaties = Relatie::orderBy('bedrijf', 'ASC')->get();;
        return view('invoer.relatie.index', compact('relaties'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdd() {
        return view('invoer.relatie.add');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdd(Request $request) {
        Relatie::create($request->all());
        return redirect()->intended('/invoer/relatie');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id) {
        $relatie = Relatie::find($id);
        return view('invoer.relatie.edit', compact('relatie'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getView($id) {
        $relatie = Relatie::find($id);
        return view('invoer.relatie.view', compact('relatie'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(Request $request, $id) {
        Relatie::find($id)->update($request->all());
        return redirect()->intended('/invoer/relatie');
    }
}
