<?php

namespace App\Http\Controllers\Invoer\Betaling;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

class BetalingController extends Controller
{

    public function __construct(Request $request) {
        $this->middleware('logedin');
    }

    public function getIndex() {
        return view('invoer.betaling.index');
    }

}
