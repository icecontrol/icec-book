<?php

namespace App\Http\Controllers\Invoer\Betaling;

use App\Betaling;
use App\CustomClasses\BetalingenService;
use App\Factuur;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

/**
 * Class CrediteurenController
 *
 * @package App\Http\Controllers\Invoer\Betaling
 */
class CrediteurenController extends Controller {

    /**
     * CrediteurenController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->middleware('logedin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex() {
        $factuurs = (new BetalingenService())->CrediteurenList();
        return view('invoer.betaling.crediteuren', compact('factuurs'));
    }
    
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBoeking($id) {
        $factuur = Factuur::find($id);
        return view('invoer.betaling.boeking', compact('factuur'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postBoeking(Request $request, $id) {
        
        $factuur = Factuur::find($id);
        
        $betaling = new Betaling();
        $betaling->factuur_id = $factuur->id;
        $betaling->grootboekrekening_id = $request->grootboekrekening_id;
        $betaling->betaaldatum = $request->betaaldatum;
        $betaling->totaal = $request->toteur;
        $betaling->valuta_id = 1;
        $betaling->save();

        if (Input::has('totfac')) {
            $betaling = new Betaling();
            $betaling->factuur_id = $factuur->id;
            $betaling->grootboekrekening_id = $request->grootboekrekening_id;
            $betaling->betaaldatum = $request->betaaldatum;
            $betaling->totaal = $request->totfac;
            $betaling->valuta_id = $factuur->Valuta->id;
            $betaling->save();
        }
        return redirect()->intended('/invoer/betaling/Crediteuren');
    }
}
