<?php

namespace App\Http\Controllers\Invoer\Betaling;

use App\Betaling;
use App\CustomClasses\BetalingenService;
use App\Factuur;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class DebiteurenController extends Controller
{
    public function __construct(Request $request) {
        $this->middleware('logedin');
    }
    
    public function getIndex() {
        $factuurs = (new BetalingenService())->DebiteurenList();
        return view('invoer.betaling.debiteuren',compact('factuurs'));
    }
    
    public function getBoeking($id) {
        $factuur = Factuur::find($id);
        return view('invoer.betaling.boeking',compact('factuur'));
    }

    public function postBoeking(Request $request, $id) {

        $factuur = Factuur::find($id);

        $betaling = new Betaling();
        $betaling->factuur_id = $factuur->id;
        $betaling->grootboekrekening_id = $request->grootboekrekening_id;
        $betaling->betaaldatum = $request->betaaldatum;
        $betaling->totaal = $request->toteur;
        $betaling->valuta_id = 1;
        $betaling->save();

        if(Input::has('totfac')){
            $betaling = new Betaling();
            $betaling->factuur_id = $factuur->id;
            $betaling->grootboekrekening_id = $request->grootboekrekening_id;
            $betaling->betaaldatum = $request->betaaldatum;
            $betaling->totaal = $request->totfac;
            $betaling->valuta_id = $factuur->Valuta->id;
            $betaling->save();
        }

        return redirect()->intended('/invoer/betaling/Debiteuren');
    }

}
