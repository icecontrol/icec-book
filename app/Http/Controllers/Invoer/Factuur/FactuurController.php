<?php

namespace App\Http\Controllers\Invoer\Factuur;

use App\Bijlage;
use App\Boeking;
use App\Factuur;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


/**
 * Class FactuurController
 *
 * @package App\Http\Controllers
 */
class FactuurController extends Controller {
    
    /**
     * InvoerController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->middleware('logedin');
    }
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex() {
        $facturen = Factuur::orderBy('factuurdatum', 'ASC')->get();
        return view('invoer.factuur.list', compact('facturen'));
    }

    public function getView($id) {
        $factuur = Factuur::find($id);
        return view('invoer.factuur.view', compact('factuur'));
    }

    public function getBijlage($id) {
        $bijlage = Bijlage::find($id);
        return view('invoer.factuur.bijlage', compact('bijlage'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id) {
        $factuur = Factuur::find($id);         
        return view('invoer.factuur.edit', compact('factuur'));
    }
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdd() {
        $factuur = new Factuur();
        return view('invoer.factuur.add', compact('factuur'));
    }
    
    
    /**
     * @param Request $request
     * @param $id
     * @return bool|\Illuminate\Http\RedirectResponse
     */
    public function postEdit(Request $request, $id) {
        if (!$request->Invoeren == 0) {
            $this->UpdateFactuur($request, $id);
            return redirect()->intended('/invoer/factuur');
        }
        return false;
    }
    
    /**
     * @param Request $request
     * @param $id
     */
    private function UpdateFactuur(Request $request, $id) {


        $factuur = Factuur::find($id);
        $factuur->relatie_id = $request->relatie_id;
        $factuur->valuta_id = $request->valuta;
        $factuur->factuurref = $request->factuurref;
        $factuur->factuurdatum = $request->factuurdatum;
        $factuur->save();
                
        foreach ($request->boeking as $boekings) {
            if (array_key_exists('id', $boekings)) {
                $boeking = Boeking::firstOrCreate(array('id' => $boekings['id']));
            } else {
                $boeking = new Boeking();
            }
            $boeking->factuur_id = $id;
            $boeking->grootboekrekening_id = $boekings['grootboekrekening_id'];
            $boeking->beschrijving = $boekings['beschrijving'];
            $boeking->totaal = floatval($boekings['totaal']);
            $boeking->save();
        }

        if (Input::file('bestand')) {
            $bestanden = Input::file('bestand');
            foreach ($bestanden as $bestand){
                $upload_data = $bestand['data'];
                $BijlageEntity = new Bijlage();
                $BijlageEntity->factuur_id = $id;
                $BijlageEntity->mimetype = $upload_data->GetMimeType();
                $BijlageEntity->data = base64_encode(file_get_contents($upload_data));
                $BijlageEntity->naam = $upload_data->GetClientOriginalName();
                $BijlageEntity->save();
            }
        }
        return;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function postAdd(Request $request) {
        if (!$request->Invoeren == 0) {
            $this->NewFactuur($request);
            return redirect()->intended('/invoer/factuur');
        }
        return false;
    }
    
    /**
     * @param Request $request
     */
    private function NewFactuur(Request $request) {

        $factuur = new Factuur();
        $factuur->relatie_id = $request->relatie_id;
        $factuur->valuta_id = $request->valuta;
        $factuur->factuurref = $request->factuurref;
        $factuur->factuurdatum = $request->factuurdatum;
        $factuur->save();
        
        foreach ($request->boeking as $boekings) {
            $boeking = new Boeking();
            $boeking->factuur_id = $factuur->id;
            $boeking->grootboekrekening_id = $boekings['grootboekrekening_id'];
            $boeking->beschrijving = $boekings['beschrijving'];
            $boeking->totaal = floatval($boekings['totaal']);
            $boeking->save();
        }

        if (Input::file('bestand')) {
            $bestanden = Input::file('bestand');
            foreach ($bestanden as $bestand){
                $upload_data = $bestand['data'];
                $BijlageEntity = new Bijlage();
                $BijlageEntity->factuur_id = $factuur->id;
                $BijlageEntity->mimetype = $upload_data->GetMimeType();
                $BijlageEntity->data = base64_encode(file_get_contents($upload_data));
                $BijlageEntity->naam = $upload_data->GetClientOriginalName();
                $BijlageEntity->save();
            }
        }
        return;
    }
    
}
