<?php

namespace App\Http\Controllers\Overzichten;

use App\CustomClasses\ResultatenRekening;
use App\CustomTraits\CalenderPeriodeTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;


/**
 * Class ResultatenrekeningController
 *
 * @package App\Http\Controllers\Overzichten
 */
class ResultatenrekeningController extends Controller {
    /**
     * @var mixed
     */
    protected $jaar;
    /**
     * @var mixed
     */
    protected $periode;

    protected $type;

    /**
     * ResultatenrekeningController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->middleware('logedin');
        $this->type = $request->type;
        $this->jaar = $request->jaar;
        $this->periode = $request->periode;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ShowResultatenrekening() {
        $resultatenrekening = (new ResultatenRekening())->BuidResultatenRekening($this->getJaar(),$this->getPeriode(), $this->getType());
        $periode = CalenderPeriodeTrait::GetCalenderPeriodes($this->getJaar(), $this->getPeriode());
        $periode_start = $periode['periodestart'];
        $periode_einde = $periode['periodeeinde'];
        $resultaten = (new ResultatenRekening())->Resultaten($periode_start, $periode_einde, $this->getType());
        $totaals = $resultaten['valutatotals'];
        $EUR = $resultaten['EUR'];
        $jaar = $this->jaar;
        $periode = $this->getPeriodeText();
        return view('overzichten.resultatenrekening', compact('resultatenrekening','totaals','EUR','jaar','periode'));
    }

    private function getPeriodeText() {
    
        switch ($this->getPeriode()) {
            case 1:
                $return='Q1';
        break;
            case 2:
                $return='Q2';
        break;
            case 3:
                $return='Q3';
        break;
            case 4:
                $return='Q4';
        break;
            case 5:
                $return='Q1-Q4';
        break;
            default;
                $return = '';
        }
        return $return;
    }
    /**
     * @return mixed
     */
    public function getJaar() {
        return $this->jaar;
    }

    /**
     * @return mixed
     */
    public function getPeriode() {
        return $this->periode;
    }
    
    /**
     * @return mixed
     */
    public function getType() {
        return $this->type;
    }
    
}
