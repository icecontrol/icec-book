<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class MainController extends Controller
{
    
    public function __construct(Request $request)
    {
        $this->middleware('logedin', ['except' => 'about']);
    }

    public function getIndex()
    {
        return view('index');
    }
    
    public function getAbout()
    {
        return view('about');
    }    
}
