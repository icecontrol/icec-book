<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Logedin {
    protected $user;

    public function __construct(Guard $user) {
        $this->user = $user;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if (\Auth::check() == true) {
            if (\Auth::check()) {
                return $next($request);
            }
        }
        return redirect()->intended('/about');
    }
}
