<?php

namespace App;

use App\CustomClasses\CurrencyConverter;
use Illuminate\Database\Eloquent\Model;

class Factuur extends Model {
    protected $table = 'factuurs';
    
    protected $fillable = [
        'factuur_referentie',
        'factuur_datum',
        'boeking',
        'totaal',
    ];
        
    public function Valuta() {
        return $this->belongsTo('App\Valuta');
    }

    public function bijlages() {
        return $this->hasMany('App\Bijlage');
    }

    public function getAllValutas() {
        return Valuta::lists('naam', 'id');
    }
    
    public function Relatie() {
        return $this->belongsTo('App\Relatie');
    }
    
    public function getAllRelaties() {
        $relaties =  Relatie::orderBy('bedrijf','ASC')->get();
        $return = array();
        foreach ($relaties as $relatie){
            $return[]=array('id' => $relatie->id,'label' => $relatie->bedrijf,'afstand' => $relatie->afstand);
        }
        return $return;
    }
    
    public function getAllGrootboekrekeningen() {
        $grootboekrekeningen = array();
        foreach (Grootboekrekening::orderBy('grootboekrekening_nummer', 'ASC')->get() as $grootboekrekening) {

            $grootboekrekeningen['Subrubriek ' . $grootboekrekening->subrubriek->getFullSubrubriekNummer() . '| ' . $grootboekrekening->subrubriek->naam][$grootboekrekening->id]
                = $grootboekrekening->getFullGrootBoekNummer() . ' | ' . $grootboekrekening->naam;
        }
        return $grootboekrekeningen;
    }

    /**
     * Gets the total amount of the factuur ,
     * getting all cost from the Boekings @ Grootboekrekeningen ,
     *
     * @return string total amount of factuur
     */
    public function getFactuurBoekingsTotaal() {
        return $this->hasMany('App\Boeking')->where('grootboekrekening_id','!=', 34)->get()->sum('totaal');
    }
    
    public function getBetaaldeFactuurTotaal() {
        return $this->hasMany('App\Betaling')->whereValutaId(1)->get()->sum('totaal');
    }


    /**
     * Once we know its not an Euro , we go to the factuurs ->boekings to find out what was paind in euros, (conversion rate)
     * if its not paid yet, do a live conversion to get an good estimate. this is last resort-
     * @return string
     */
    public function getBetaaldeFactuurEuroTotaalOrAnEstimate() {
        $betaalde = $this->hasMany('App\Betaling')->whereValutaId(1)->get()->sum('totaal');
        if($betaalde > 0){
            return $betaalde;
        }else{
            return (new CurrencyConverter())->currency($this->Valuta->naam, 'EUR', $this->hasMany('App\Boeking')->get()->sum('totaal'));
        }
    }

    /**
     * @return string
     */
    public function GetOpenstaandBedrag(){        
        return $this->BetaaldeFactuurTotaal($this) - $this->FactuurBoekingsTotaal($this);
    }
    /**
     * @param Factuur $factuur
     * @return string
     */
    private function BetaaldeFactuurTotaal(Factuur $factuur) {
        return $factuur->betaling()->get()->sum('totaal');
    }

    public function betaling() {
        return $this->hasMany('App\Betaling');
    }

    /**
     * @param Factuur $factuur
     * @return string
     */
    public function FactuurBoekingsTotaal(Factuur $factuur) {
        return $factuur->boekings()->get()->sum('totaal');
    }

    public function boekings() {
        return $this->hasMany('App\Boeking');
    }
    
    public function kmboeking() {
        return $this->hasMany('App\Boeking')->where('grootboekrekening_id',34)->get();
    }

     public function getAllFinancielerekeningen() {
        $grootboekrekeningen = array();
        foreach (Grootboekrekening::orderBy('grootboekrekening_nummer', 'ASC')->get() as $grootboekrekening) {
            if ($grootboekrekening->subrubriek->rubriek->rubrieks_nummer == 1) {
                $grootboekrekeningen['Subrubriek ' . $grootboekrekening->subrubriek->getFullSubrubriekNummer() . '| ' . $grootboekrekening->subrubriek->naam][$grootboekrekening->id]
                    = $grootboekrekening->getFullGrootBoekNummer() . ' | ' . $grootboekrekening->naam;
            }
        }
        return $grootboekrekeningen;
    }

}
