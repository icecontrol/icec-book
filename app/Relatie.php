<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relatie extends Model
{
    //
    protected $table = 'relaties';

    /**
     * @var array
     */
    protected $fillable = [
        'bedrijf',
        'btw',
        'bank',
        'telefoon',
        'email',
        'website',
        'contact',
        'straat',
        'postcode',
        'plaats',
        'land',
        'afstand',
    ];
    public function factuur()
    {
        return $this->hasMany('App\Factuur');
    }
}
