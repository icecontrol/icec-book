<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rubriek
 *
 * @package App
 */
class Rubriek extends Model {
    /**
     * @var string
     */
    protected $table = 'rubrieks';
    
    /**
     * @var array
     */
    protected $fillable = [
        'rubrieks_nummer',
        'naam',
        'type',
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subrubrieken() {
        return $this->hasMany('App\SubRubriek', 'rubrieks_id', 'id');
    }
}
