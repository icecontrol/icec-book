<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bijlage extends Model
{
    //
    protected $table = 'bijlages';

    protected $fillable = [
        'id',
        'factuur_id',
        'mimetype',
        'data',
        'naam',
    ];
    
    public function factuur()
    {
        return $this->belongsTo('App\Factuur');
    }
}
