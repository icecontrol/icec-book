<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SubRubriek
 *
 * @package App
 */
class SubRubriek extends Model {
    /**
     * @var string
     */
    protected $table = 'sub_rubrieks';
    
    /**
     * @var array
     */
    protected $fillable = [
        'rubrieks_id',
        'sub_rubrieks_nummer',
        'naam',
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rubriek() {
        return $this->belongsTo('App\Rubriek', 'rubrieks_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function grootboekrekeningen() {
        return $this->hasMany('App\Grootboekrekening','sub_rubrieks_id','id');
    }
    
    public function getFullSubrubriekNummer()
    {
        return $this->rubriek->rubrieks_nummer. $this->sub_rubrieks_nummer ;
    }
}
