@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        @include('flash::message')
        <div class="row">
            <div class="col-md-12 Overzichten">
                <h1>Uitgaves
                    <small>Q-{{ ceil(date('n', time()) / 3)}} {{date('Y')}}</small>
                </h1>
                <div id="uitgaveschart"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 Overzichten">
                <h1>Inkomsten
                    <small>Q-{{ ceil(date('n', time()) / 3)}} {{date('Y')}}</small>
                </h1>
                <div id="inkomstenchart"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 Overzichten">
                <h1>Winst & Verlies
                    <small>per kwartaal</small>
                </h1>
                <div id="winstverlieschart"></div>
            </div>
        </div>
    </div>
    <style>
        #uitgaveschart, #inkomstenchart {
            /*width: 100%;*/
            height: 700px;
            font-size: 11px;
            background: rgb(63, 63, 79) none repeat scroll 0 0;
            color: rgb(255, 255, 255);
            border-radius: 15px;
            /*padding: 20px;*/
        }

        #winstverlieschart {
            /*width: 100%;*/
            height: 500px;
            font-size: 11px;
            background: rgb(63, 63, 79) none repeat scroll 0 0;
            color: rgb(255, 255, 255);
            border-radius: 15px;
        }

        .amcharts-pie-slice {
            transform: scale(1);
            transform-origin: 50% 50%;
            transition-duration: 0.3s;
            transition: all .3s ease-out;
            -webkit-transition: all .3s ease-out;
            -moz-transition: all .3s ease-out;
            -o-transition: all .3s ease-out;
            cursor: pointer;
            box-shadow: 0 0 30px 0 #000;
        }

        .amcharts-pie-slice:hover {
            transform: scale(1.1);
            /*filter: url(#shadow);*/
        }
    </style>



    <script>
        MakeUitgavesChart();
        MakeWinstEnVerliesChart();

        var slideIndex = 0;
//        carousel();

        function carousel() {
            var i;
            var x = document.getElementsByClassName("Overzichten");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            slideIndex++;
            if (slideIndex > x.length) {
                slideIndex = 1
            }
            x[slideIndex - 1].style.display = "block";
            setTimeout(carousel, 8000); // Change image every 8 seconds
        }

        function MakeUitgavesChart() {
            $.getJSON("/api/huidige-periode-resultaten", null, function (data) {
                DrawUitgavesChart();
                DrawInkomstenChart();

                function DrawInkomstenChart() {
                    var inkomstenchart = AmCharts.makeChart("inkomstenchart", {
                        "type": "pie",
                        "startDuration": 0,
                        "theme": "dark",
                        "addClassNames": true,
                        "legend": {
                            "position": "bottom",
                            "marginLeft": 0,
                            "marginTop": 0,
                            "autoMargins": false
                        },
                        "innerRadius": "30%",
                        "defs": {
                            "filter": [{
                                "id": "shadow",
                                "width": "200%",
                                "height": "200%",
                                "feOffset": {
                                    "result": "offOut",
                                    "in": "SourceAlpha",
                                    "dx": 0,
                                    "dy": 0
                                },
                                "feGaussianBlur": {
                                    "result": "blurOut",
                                    "in": "offOut",
                                    "stdDeviation": 5
                                },
                                "feBlend": {
                                    "in": "SourceGraphic",
                                    "in2": "blurOut",
                                    "mode": "normal"
                                }
                            }]
                        },
                        "dataProvider": data.inkomstenarray,
                        "valueField": "totaal",
                        "titleField": "grootboek",
                        "export": {
                            "enabled": true
                        }
                    });

                    inkomstenchart.addListener("init", handleInitInkomsten);
                    inkomstenchart.addListener("rollOverSlice", function (e) {
                        handleRollOver(e);
                    });

                    function handleInitInkomsten() {
                        inkomstenchart.legend.addListener("rollOverItem", handleRollOver);
                    }
                }

                function DrawUitgavesChart() {
                    var uitgaveschart = AmCharts.makeChart("uitgaveschart", {
                        "type": "pie",
                        "startDuration": 0,
                        "theme": "dark",
                        "addClassNames": true,
                        "legend": {
                            "position": "bottom",
                            "marginLeft": 0,
                            "marginTop": 0,
                            "autoMargins": false
                        },
                        "innerRadius": "30%",
                        "defs": {
                            "filter": [{
                                "id": "shadow",
                                "width": "200%",
                                "height": "200%",
                                "feOffset": {
                                    "result": "offOut",
                                    "in": "SourceAlpha",
                                    "dx": 0,
                                    "dy": 0
                                },
                                "feGaussianBlur": {
                                    "result": "blurOut",
                                    "in": "offOut",
                                    "stdDeviation": 5
                                },
                                "feBlend": {
                                    "in": "SourceGraphic",
                                    "in2": "blurOut",
                                    "mode": "normal"
                                }
                            }]
                        },
                        "dataProvider": data.uitgavesarray,
                        "valueField": "totaal",
                        "titleField": "grootboek",
                        "export": {
                            "enabled": true
                        }
                    });

                    uitgaveschart.addListener("init", handleInitUitgave);
                    uitgaveschart.addListener("rollOverSlice", function (e) {
                        handleRollOver(e);
                    });

                    function handleInitUitgave() {
                        uitgaveschart.legend.addListener("rollOverItem", handleRollOver);
                    }
                }

                function handleRollOver(e) {
                    var wedge = e.dataItem.wedge.node;
                    wedge.parentNode.appendChild(wedge);
                }
            });
        }

        function MakeWinstEnVerliesChart() {
            $.getJSON("/api/winst-verlies-resultaten", null, function (data) {

                var winstverlieschart = AmCharts.makeChart("winstverlieschart", {
                    "type": "serial",
                    "theme": "dark",
                    "precision": 2,
                    "valueAxes": [{
                        "id": "v1",
                        "title": "Inkomsten & Uitgaves",
                        "position": "left",
                        "autoGridCount": false,
                        "labelFunction": function (value) {
                            return "€ " + Math.round(value);
                        }
                    }, {
                        "id": "v2",
                        "title": "Winst & Verlies",
                        "gridAlpha": 0,
                        "position": "right",
                        "autoGridCount": false,
                        "labelFunction": function (value) {
                            return "€ " + Math.round(value);
                        }
                    }],
                    "graphs": [
                        {
                            "id": "g3",
                            "valueAxis": "v1",
                            "lineColor": "#f78217",
                            "fillColors": "#e1ede9",
                            "fillAlphas": 1,
                            "type": "column",
                            "title": "Uitgaves",
                            "valueField": "uitgaves",
                            "clustered": true,
                            "columnWidth": 0.5,
                            "legendValueText": "€ [[value]]",
                            "balloonText": "[[title]]<br/><b style='font-size: 130%'>€ [[value]]</b>"
                        },
                        {
                            "id": "g4",
                            "valueAxis": "v1",
                            "lineColor": "#62cf73",
                            "fillColors": "#62cf73",
                            "fillAlphas": 1,
                            "type": "column",
                            "title": "Inkomsten",
                            "valueField": "inkomsten",
                            "clustered": true,
                            "columnWidth": 0.5,
                            "legendValueText": "€ [[value]]",
                            "balloonText": "[[title]]<br/><b style='font-size: 130%'>€ [[value]]</b>"
                        },
                        {
                            "id": "g2",
                            "valueAxis": "v2",
                            "bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletColor": "#FFFFFF",
                            "bulletSize": 5,
                            "hideBulletsCount": 50,
                            "lineThickness": 2,
                            "lineColor": "red",
                            "type": "smoothedLine",
                            "dashLength": 5,
                            "title": "Winst / Verlies",
                            "useLineColorForBulletBorder": true,
                            "valueField": "winstverlies",
                            "balloonText": "[[title]]<br/><b style='font-size: 130%'>€ [[value]]</b>"
                        }],
                    "chartScrollbar": {
                        "graph": "g1",
                        "oppositeAxis": false,
                        "offset": 30,
                        "scrollbarHeight": 50,
                        "backgroundAlpha": 0,
                        "selectedBackgroundAlpha": 0.1,
                        "selectedBackgroundColor": "#888888",
                        "graphFillAlpha": 0,
                        "graphLineAlpha": 0.5,
                        "selectedGraphFillAlpha": 0,
                        "selectedGraphLineAlpha": 1,
                        "autoGridCount": true,
                        "color": "#AAAAAA"
                    },
                    "chartCursor": {
                        "pan": true,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "cursorAlpha": 0,
                        "valueLineAlpha": 0.2
                    },
                    "categoryField": "periode",
                    "categoryAxis": {
                        "parseDates": false,
                        "dashLength": 1,
                        "minorGridEnabled": true
                    },
                    "legend": {
                        "useGraphSettings": true,
                        "position": "top"
                    },
                    "balloon": {
                        "borderThickness": 1,
                        "shadowAlpha": 0
                    },
                    "export": {
                        "enabled": true
                    },
                    "dataProvider": data.winst_verlies_array
                });
            });
        }
    </script>
@stop
