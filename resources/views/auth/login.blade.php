@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        {!! Form::open() !!}
        <br>
        <br>
        <div class="row">
            <div class="col-xs-5">
                <blockquote>
                    <h1 class="featurette-heading">Prive Boekhouder</h1>
                    <small>by I.C.E.C - <cite title="Source Title">ICE Control &copy; {{date('Y')}}</cite></small>
                    <p>OpenSource boekhouding voor Zzp'ers <a href="https://bitbucket.org/icecontrol/icec-book">https://bitbucket.org/icecontrol/icec-book</a>
                    </p>

                </blockquote>
            </div>
        </div>
        <hr>
        <div class="row">
            @include('partials.validationerrors')
            <div class="col-xs-offset-3 col-xs-6">
                <div class="row form-group">
                    <div class="row">
                        <div class="input-group margin-bottom-sm">
                            <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                            {!!	Form::text('email', old('email'), array('class' =>'form-control', 'placeholder' => ' Email address'))	!!}
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                            {!!	Form::password('password',array('class' =>'form-control', 'placeholder' => ' Wachtwoord'))	!!}
                        </div>
                        <br>
                        <div class="control-group">
                            <div>
                                <button type="submit" class="btn btn-primary btn-large2" name="checkfa">
                                    Login
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop




