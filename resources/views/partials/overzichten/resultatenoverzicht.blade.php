@if((count($totaals)) > 0)
    <div class="well">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Grootboek</th>
                    <th>Inkomst</th>
                    <th>Uitgave</th>
                </tr>
            </thead>
            <tbody>
                @foreach($totaals as $grootboek => $boekings)
                    @foreach($boekings as $inkomst_uitgave => $valutas)
                        @foreach($valutas as $valuta => $bedrag)
                            @if($inkomst_uitgave == 0)
                                <tr class="danger">
                                    <td><?= $grootboek?></td>
                                    <td></td>
                                    <td>- <?= $valuta?> <?= number_format($bedrag, 2, '.', ',')?></td>
                                </tr>
                            @endif
                            @if($inkomst_uitgave == 1)
                                <tr class="success">
                                    <td><?= $grootboek?></td>
                                    <td>+ <?= $valuta?> <?= number_format($bedrag, 2, '.', ',')?></td>
                                    <td></td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                @endforeach
            </tbody>
        </table>
        <hr>
        <div class="row">
            <div class="col-xs-offset-8 col-xs-2">
                <h2>Resultaat |</h2>
            </div>
                <div class="col-xs-2">
                    <br>
                <div class="row">
                    <h4>EUR = <?= number_format($EUR, 2, '.', ',') ?></h4>
                </div>
            </div>
        </div>
    </div>
@endif
