@if((count($resultatenrekening)) > 0)
    @foreach($resultatenrekening as $key => $rubriek)
        <div class="well">
            <h2><?= $key ?></h2>
            <br>
            @include('partials.overzichten.subrubriek')
        </div>
    @endforeach
@endif

