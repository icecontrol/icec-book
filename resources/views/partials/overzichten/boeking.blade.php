@if((count($grootboekrekening)) > 0)
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Datum</th>
                <th>Ref</th>
                <th>Relatie</th>
                <th>Factuur Bedrag</th>
                <th>Boek Bedrag</th>
            </tr>
        </thead>
        <tbody>
            @foreach($grootboekrekening as $key => $boeking)
                <tr>
                    <td><?= $boeking->factuur->factuurdatum ?></td>
                    <td><a href="/invoer/factuur/view/<?= $boeking->factuur->id ?>"><?= $boeking->factuur->factuurref ?></a></td>
                    <td><a href="/invoer/relatie/view/<?= $boeking->factuur->Relatie->id ?>"><?= $boeking->factuur->Relatie->bedrijf ?></a>
                    </td>
                    <td><?= $boeking->factuur->Valuta->symbool ?> <?= number_format($boeking->factuur->getFactuurBoekingsTotaal(), 2, '.', ',') ?></td>
                    <td> &euro;
                        @if($boeking->factuur->Valuta->id == 1)
                        <?= number_format($boeking->totaal, 2, '.', ',')  ?>
                        @else
                            <?= number_format($boeking->factuur->getBetaaldeFactuurEuroTotaalOrAnEstimate(), 2, '.', ',')  ?>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <hr>
    @foreach($grootboekrekening as $key => $boeking)
        <?php
        if($boeking->factuur->Valuta->id == 1){
            $totals[] = $boeking->totaal ;
        }else{
            $tota = $boeking->factuur->getBetaaldeFactuurEuroTotaalOrAnEstimate() ;
            $totals[] = $tota ;
       }
        ?>
    @endforeach
    <div class="row">
            <div class="col-xs-2" style="float: right">
                <span><b>Totaal  =  &euro; <?= number_format(array_sum($totals), 2, '.', ',') ?></b></span>
            </div>
    </div>
@endif
