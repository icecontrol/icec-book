@if((count($subrubriek)) > 0)
    @foreach($subrubriek as $key => $grootboekrekening)
        <div class="well">
            <h4><?= $key ?></h4>
            <br>
            @include('partials.overzichten.boeking')
        </div>
    @endforeach
@endif
