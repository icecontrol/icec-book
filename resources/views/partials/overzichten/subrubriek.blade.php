
@if((count($rubriek)) > 0)
    @foreach($rubriek as $key => $subrubriek)
        <div class="well">
            <h3><?= $key ?></h3>
            <br>
        @include('partials.overzichten.grootboek')
        </div>
    @endforeach
@endif
