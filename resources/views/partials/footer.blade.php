<footer class="footer">
    <div class="container">
        <p class="text-muted" style="float: right;">Built by I.C.E.C - ICE Control &copy; {{date('Y')}}</p>
    </div>
</footer>