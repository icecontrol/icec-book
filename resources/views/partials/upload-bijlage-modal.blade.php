<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Bijlage toevoegen</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['files' => true ]) !!}
                {!!   Form::hidden('factuur_id', $factuur->id) !!}
                {!!   Form::file('bijlage', old('bijlage', array('class' => 'btn'))) !!}
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-lg btn-block" name="upload"  onclick="return confirm('Factuur invoeren ?');">
                    Upload Bijlage
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>