<nav class="navbar navbar-default">
    <div class="container-fluid">

        @if(Auth::check())
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><span class="glyphicon glyphicon-home fa-lg"></span></a>
        </div>
        @endif
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                @if(Auth::check())
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="-" aria-expanded="false"><span class="glyphicon glyphicon-pencil fa-lg"></span> <span
                                class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/invoer/factuur">Facturen</a></li>
                        <li><a href="/invoer/betaling">Betaling</a></li>
                        <li><a href="/invoer/relatie">Relatie</a></li>
                    </ul>
                </li>

                <li><a href="/overzichten">Overzichten</a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="-" aria-expanded="false">Producten
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/product/create">Toevoegen</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/product">Lijst</a></li>
                    </ul>
                </li>

                <li><a href="/instelling"><span class="glyphicon glyphicon-wrench fa-lg"></span></a></li>
                @endif
                    @if(!Auth::check())
                <li><a href="/about"><span class="glyphicon glyphicon-info-sign fa-lg"></span></a></li>
                    @endif
            </ul>
        </div>
    </div>
</nav>
