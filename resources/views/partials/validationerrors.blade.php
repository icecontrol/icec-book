@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Oeps!</strong> de volgende veld(en) zijn vereist :<br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
