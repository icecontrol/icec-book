{{ Form::model($factuur) }}

<div class="row">
    <div class="col-xs-6">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-5">
                {{ Form::label('Relatie', 'Relatie') }}
            </div>
            <div class="col-xs-6">
                <span><a href="/invoer/relatie/view/<?= $factuur->Relatie->id ?>"><?= $factuur->Relatie->bedrijf ?></a></span>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-offset-1 col-xs-5">
                {{ Form::label('Factuur Referentie', 'Factuur Referentie') }}
            </div>
            <div class="col-xs-6">
                <span>{{$factuur->factuurref}}</span>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-offset-1 col-xs-5">
                {{ Form::label('Factuur datum', 'Factuur datum') }}
            </div>
            <div class="col-xs-6">
                <span>{{$factuur->factuurdatum}}</span>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-offset-1 col-xs-5">
                {{ Form::label('Factuur Valuta', 'Factuur Valuta') }}
            </div>
            <div class="col-xs-6">
                <span>{{$factuur->Valuta->naam}}</span>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-offset-1 col-xs-5">
                {{ Form::label('Factuur Valuta', 'Km vergoeding') }}
            </div>
            <div class="col-xs-6">
                        <span>
                            @if($factuur->km ==0)
                                Nee
                            @else
                                Ja
                            @endif
                            </span>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-4">
                @if(count($factuur->bijlages) > 0 )
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="-"
                                    aria-expanded="false">Bijlages </span>
                                <span class="btn btn-warning glyphicon glyphicon-search"></span></a>
                            <ul class="dropdown-menu">
                                @foreach($factuur->bijlages as $bijlage)
                                    <li><a href="/invoer/factuur/bijlage/{!! $bijlage->id !!}" target="_blank">{!! $bijlage->naam !!}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                @else
                    Geen Bijlages
                @endif
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-xs-offset-1 col-xs-10">
        <span class="h4 -bold">Boekingen</span>
        <div class="row">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Grootboekrekening</th>
                        <th>Beschrijving</th>
                        <th>Subtotaal</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($factuur->boekings as $key => $boeking)
                        <tr>
                            <td><?= $boeking->grootboekrekening->getFullGrootBoekNummer() . ' | ' . $boeking->grootboekrekening->naam ?></td>
                            <td><?= $boeking->beschrijving ?></td>
                            <td><?= $factuur->Valuta->symbool ?> <?= number_format($boeking->totaal, 2, '.', ',') ?></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-xs-offset-9 col-xs-3">
                {!!	Form::label('ftotaal','Factuur totaal')	!!}
                <label for="totaal_sum">
                    = <?= $factuur->Valuta->symbool ?> <?= number_format($factuur->getFactuurBoekingsTotaal(), 2, '.', ',')?> </label>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-xs-offset-1 col-xs-10">
        <span class="h4 -bold">Betalingen</span>
        <div class="row">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Grootboekrekening</th>
                        <th>Datum</th>
                        <th>Subtotaal</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($factuur->betaling) > 0)
                        @foreach($factuur->betaling as $key => $boeking)
                            <tr>
                                <td><?= $boeking->grootboekrekening->getFullGrootBoekNummer() . ' | ' . $boeking->grootboekrekening->naam ?></td>
                                <td><?= date("d-m-Y", strtotime($boeking->betaaldatum))   ?></td>
                                <td><?= $boeking->Valuta->symbool ?> <?= number_format($boeking->totaal, 2, '.', ',') ?></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-xs-offset-9 col-xs-3">
                {!!	Form::label('ftotaal','Betalingen totaal')	!!}
                <label for="totaal_sum">
                    = &euro; <?= number_format($factuur->getBetaaldeFactuurTotaal(), 2, '.', ',')?> </label>
            </div>
        </div>
    </div>
</div>
<hr>
<br>
<br>
{!! Form::close() !!}
