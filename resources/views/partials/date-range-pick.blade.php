{{--{!! Form::open() !!}--}}
<div class="row-fluid">
    <div class="panel-heading"><h1>{{$title}}
            @if(isset($kwartaal))
                Q{{$kwartaal}}
            @endif
            @if(isset($jaar))
                {{$jaar}}
            @endif

        </h1></div>
</div>

<div class="row">
    <div class="col-sm-1">
        {!!	Form::label('periode','Periode')	!!}
    </div>
    <div class="col-sm-1">
        <select id="periode" name="periode">
            @if(isset($kwartaal))
                <option value="{{$kwartaal}}" selected>Q{{$kwartaal}}</option>
                {{$kwartaal}}
            @endif
            <option value="1">Q1</option>
            <option value="2">Q2</option>
            <option value="3">Q3</option>
            <option value="4">Q4</option>
            <option value="5">Q1-4</option>
        </select>
    </div>
    <div class="col-sm-1">
        <select name="jaar">
            @if(isset($jaar))
                <option value="{{$jaar}}" selected>{{$jaar}}</option>
                {{$jaar}}
            @endif
            <option value="2012">2012</option>
            <option value="2013">2013</option>
            <option value="2014">2014</option>
            <option value="2015">2015</option>
            <option value="2016">2016</option>
        </select>
    </div>
    <div class="col-sm-1">
        <button type="submit" class="btn btn-primary btn-large2">
            Toon Overzicht
        </button>
    </div>
</div>

{{--{!! Form::close() !!}--}}