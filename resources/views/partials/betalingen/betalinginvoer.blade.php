{!! Form::open() !!}
<div class="well">
    <div class="row">
        <div class="col-sm-offset-1 col-sm-2">
            <div class="control-group">
                {!!	Form::label('betaaldatum','Overboekings datum')	!!}
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-actions">
                {!!	Form::input('date', 'betaaldatum', old('betaaldatum'), array('class' => 'form-control','placeholder' => 'Kies overboekingsdatum','require'))	!!}
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-sm-offset-1 col-sm-2">
            {!!	Form::label('grootboekrekening_id','Financiele rekening')	!!}<br>

        </div>
        <div class="col-sm-3">
            {{ Form::select('grootboekrekening_id', $factuur->getAllFinancielerekeningen(), null, array('class' => 'form-control','placeholder' => 'Kies Financiele rekening','require')) }}
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-offset-1 col-sm-2">
            {!!	Form::label('toteur','Totaal in EUR ')	!!}
        </div>
        <div class="col-sm-3">
            {!!	Form::text('toteur', $factuur->getFactuurBoekingsTotaal(),  array('class' => 'form-control','placeholder' => 'Totaal in EUR','require'))	!!}
        </div>
    </div>
    <br>

    @if($factuur->Valuta->id != 1)
        <div class="row">
            <div class="col-sm-offset-1 col-sm-2">
                {!!	Form::label('totfac','Totaal in '.$factuur->Valuta->naam)	!!}
            </div>
            <div class="col-sm-3">
                {!!	Form::text('totfac', null,  array('class' => 'form-control','placeholder' => 'Totaal in '.$factuur->Valuta->naam,'require'))	!!}
            </div>
        </div>
    @endif
    <hr>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-8">
            <button type="submit" class="btn btn-primary btn-lg btn-block" name="Invoeren" value="Invoeren"
                    onclick="return confirm('Factuur invoeren ?');">
                Invoeren
            </button>
        </div>
    </div>

    {!! Form::close() !!}
    <br>
    <br>
</div>