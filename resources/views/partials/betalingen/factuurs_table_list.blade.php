<div class="control-group">
    <h3>Factuur</h3>
    <div class="row-fluid">
        @if(isset($factuurs))
            <table id="FactuursList" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr style=>
                    <td><b>Referentie</b></td>
                    <td><b>Factuur datum</b></td>
                    <td><b>Relatie</b></td>
                    <td><b>Valuta</b></td>
                    <td><b>Factuur totaal</b></td>
                    <td><b>Openstaand</b></td>
                    <td><b>Actie</b></td>
                </tr>
                </thead>
                <tbody>
                @if(count($factuurs) > 0)
                @foreach ($factuurs as $factuur)
                    <tr>
                        <td><a href="/invoer/factuur/view/<?= $factuur->id ?>"><?= $factuur->factuurref ?></a></td>
                        <td>{{ $factuur->factuurdatum  }}</td>
                        <td><a href="/invoer/relatie/view/<?= $factuur->Relatie->id ?>"><?= $factuur->Relatie->bedrijf ?></a>
                        </td>
                        <td>{{ $factuur->Valuta->naam }}</td>
                        <td>{{ $factuur->Valuta->symbool }} {{ $factuur->getFactuurBoekingsTotaal() }}</td>
                        <td>{{ $factuur->Valuta->symbool }} {{ $factuur->GetOpenstaandBedrag() }}</td>
                        <td><a href="/invoer/betaling/Crediteuren/boeking/<?= $factuur->id ?>"><span class="btn btn-warning ">boek</span></td>
                    </tr>
                @endforeach

                @else
                    <tr class="success">
                        <td colspan="7">Geen open facturen</td>
                    </tr>
                @endif
                </tbody>
            </table>
        @endif
        <hr>
    </div>
</div>
<script>
    $(function () {
        if($('#FactuursList').find('tbody tr').length > 1){
            $('#FactuursList').dataTable({
                "order": [[ 1, "asc" ]],
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "bSortable": false },
                ]
            });
        }

    });
</script>