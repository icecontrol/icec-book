<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Boekhouding </title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta name='description' content=''>
    <meta name='author' content='I.C.E.C'>
    {!! Html::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',array('integrity' => 'sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7','crossorigin' => 'anonymous')) !!}

    {!! Html::style('css/justified-nav.css') !!}
    {!! Html::style('css/bootstrap-datepicker.css') !!}
    {!! Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css') !!}


    {!! Html::script('https://code.jquery.com/jquery-2.2.4.min.js') !!}
    {!! Html::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',array('integrity' => 'sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS','crossorigin' => 'anonymous')) !!}


    <link href="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>


    {!! Html::script('js/bootstrap-datepicker.js') !!}
    {!! Html::script('https://www.amcharts.com/lib/3/amcharts.js') !!}
    {!! Html::script('https://www.amcharts.com/lib/3/pie.js') !!}
    {!! Html::script('https://www.amcharts.com/lib/3/serial.js') !!}
    {!! Html::script('https://www.amcharts.com/lib/3/themes/dark.js') !!}
</head>
<body>
@yield('content')
@include('partials.footer')
</body>

</html>
