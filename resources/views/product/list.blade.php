@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')

        <div class="row">
            @include('flash::message')

            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="col-lg-1">artikel nr</th>
                            <th class="col-lg-2">Naam</th>
                            <th>kostprijs(10st)</th>
                            <th>kostprijs(25st)</th>
                            <th>kostprijs(50st)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Table cell</td>
                            <td>Table cell</td>
                            <td>Table cell</td>
                            <td>Table cell</td>
                            <td>Table cell</td>
                            <td>Table cell</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@stop
