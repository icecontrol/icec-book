@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <br>
        <div class="row">

            <div class="col-xs-10">
                <h2>Relatie | {{$relatie->bedrijf}}</h2>
            </div>
        </div>
        <hr>
        <br>
        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('bedrijf', 'Naam') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->bedrijf}}</span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('btw', 'BTW') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->btw }}</span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('bank', 'Bank') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->bank }}</span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('telefoon', 'Telefoon') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->telefoon }}</span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('email', 'E-mail') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->email }}</span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('website', 'Website') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->website }}</span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('contact', 'Contact') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->contact }}</span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('straat', 'Straat') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->straat }}</span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('postcode', 'Postcode') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->postcode }}</span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('plaats', 'Plaats') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->plaats }}</span>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('land', 'Land') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->land }}</span>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-offset-1 col-xs-2">
                {{ Form::label('afstand', 'Afstand vanavaf werklocatie') }}
            </div>
            <div class="col-xs-9">
                <span>{{$relatie->afstand }} km</span>
            </div>
        </div>
        <br>
        <hr>
        <div class="row">
            @include('flash::message')
        </div>
    </div>
@stop
