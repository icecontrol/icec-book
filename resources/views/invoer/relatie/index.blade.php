@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="row-fluid">
            <div class="panel-heading"><h1>Relaties <a href="{{url()->current().'/add'}}"><span class=" btn btn-success glyphicon glyphicon-plus" style="float: right"></span></a></h1></div>
            <hr>
        </div>

        <div class="col-xs-offset-1 col-xs-10">
            <div class="row">
                <table id="RelatiesList" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="col-xs-3">Naam</th>
                            <th class="col-xs-3">Website</th>
                            <th class="col-xs-2">Tel</th>
                            <th class="col-xs-2">Plaats</th>
                            <th class="col-xs-2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($relaties as $relatie)
                            <tr>
                                <th>{{$relatie->bedrijf}}</th>
                                <th>{{$relatie->website}}</th>
                                <th>{{$relatie->telefoon}}</th>
                                <th>{{$relatie->plaats}}, {{$relatie->land}}</th>
                                <th>
                                    <a class="btn btn-xs btn-success btnAdd" href="/invoer/relatie/view/{{ $relatie->id }}">
                                        <span class="glyphicon glyphicon-search"></span>
                                        View
                                    </a>
                                    <a class="btn btn-xs btn-warning btnAdd" href="/invoer/relatie/edit/{{$relatie->id }}">
                                        <span class="glyphicon glyphicon-edit"></span>
                                        Edit
                                    </a>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <br>
    </div>
    <script>
        $(function () {
            $('#RelatiesList').dataTable({
                "order": [[ 1, "asc" ]],
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    { "bSortable": false },
                ]
            });
        });
    </script>
@stop
