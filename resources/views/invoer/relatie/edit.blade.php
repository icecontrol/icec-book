@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <br>
         {{ Form::model($relatie) }}
        <div class="row">

            <div class="col-xs-10">
                <h2>Relatie | {{$relatie->bedrijf}}</h2>
            </div>
        </div>
        <hr>
        <br>
        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('bedrijf', 'Naam') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('bedrijf',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('btw', 'BTW') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('btw',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('bank', 'Bank') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('bank',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('telefoon', 'Telefoon') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('telefoon',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('email', 'E-mail') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('email',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('website', 'Website') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('website',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('contact', 'Contact') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('contact',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('straat', 'Straat') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('straat',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('postcode', 'Postcode') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('postcode',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('plaats', 'Plaats') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('plaats',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('land', 'Land') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('land',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('afstand', 'Afstand vanavaf werklocatie') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('afstand',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>
        <hr>
        {{ Form::submit('Update', array('class' => 'form-control btn btn-primary')) }}
        {{ Form::close() }}
        <hr>
        <div class="row">
            @include('flash::message')
        </div>
    </div>
@stop
