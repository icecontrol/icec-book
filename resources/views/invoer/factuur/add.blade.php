@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="hero-unit">
            <div class="title"><h1></h1></div>
            <div class="hero-unit">
                <div class="row-fluid">
                    <div class="panel-heading"><h1>Factuur invoer </h1></div>
                    <hr>

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                </div>
                <br>
                {!! Form::open(['files'=>'true']) !!}
                <div class="row form-group">
                    <div class="col-sm-4 Relatie">
                        <div class="control-group">
                            <label for="relatie_id">Relatie</label><br>
                            <select name="relatie_id" id="relatie_id" class="form-control" required>
                                <option value="" class="" selected> Kies Relatie</option>
                                @foreach($factuur->getAllRelaties() as $relatie)
                                    <option value="{!! $relatie['id'] !!}" data-afstand="{!! $relatie['afstand'] !!}">{!! $relatie['label'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="control-group">
                            {!!	Form::label('valuta','Valuta')	!!}<br>
                            {{ Form::select('valuta', $factuur->getAllValutas(), $factuur->valuta_id, array('class' => 'form-control', 'required' => true)) }}
                        </div>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-sm-4">
                        <div class="control-group">
                            {!!	Form::label('factuurref','Factuur # / referentie')	!!}<br>
                            {!!	Form::text('factuurref', old('factuurref'), array('placeholder' => 'Referentie','class' => 'form-control'))	!!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="control-group">
                            {!!	Form::label('factuurdatum','Factuur datum')	!!}<br>
                            <input type="text" placeholder="Factuur datum" id="factuurdatum" name="factuurdatum" class="form-control" required>
                        </div>
                    </div>
                </div>
                <br>

                <div class="row form-group">
                    <div class="row">
                        <div class="col-sm-8">
                            <span class="h1">Boekings <a class="boeking_toevoegen btn btn-success glyphicon glyphicon-plus"></a></span>
                        </div>
                        <div class="col-sm-2">
                            <a class="btw_toevoegen btn btn-success glyphicon glyphicon-plus"> btw</a>
                        </div>
                        <div class="col-sm-2">
                            <a class="km_toevoegen btn btn-success glyphicon glyphicon-plus"> km</a>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <fieldset class="Boekings">
                            <div class="row form-group">
                                <div class="col-sm-3"><label for="" class="">Grootboekrekening</label></div>
                                <div class="col-sm-5"><label for="">Beschrijving</label></div>
                                <div class="col-sm-4"><label for="">Subtotaal</label></div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <hr>

                <div class="row form-group">
                    <div class="col-sm-offset-9 col-sm-2">
                        {!!	Form::label('ftotaal','Factuur totaal')	!!}
                    </div>
                    <div class="col-sm-1">
                        <label for="totaal_sum">0.00 </label> €
                    </div>

                </div>

                <div class="row form-group">

                    <div class="row">
                        <div class="col-sm-12">
                            <span class="h1">Bestanden <a class="bestand_toevoegen btn btn-success glyphicon glyphicon-plus"></a></span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <fieldset class="Bestanden">
                        </fieldset>
                    </div>
                </div>
                <hr>
                <br>
                <br>
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-8">
                        <button type="submit" class="btn btn-primary btn-lg btn-block" name="Invoeren" value="Invoeren"
                                onclick="return confirm('Factuur invoeren ?');">
                            Invoeren
                        </button>
                    </div>
                </div>

                <fieldset class="Boeking BoekingTemplate" style="display: none;" disabled>

                    <div class="col-sm-3">
                        {{ Form::select('boeking[0][grootboekrekening_id]', $factuur->getAllGrootboekrekeningen(), null, array('class' => 'form-control','placeholder' => 'Kies grootboekrekening', 'required' => true)) }}
                    </div>

                    <div class="col-sm-5">
                        {!!	Form::text('boeking[0][beschrijving]', null, array('placeholder' => 'Referentie','class' => 'form-control'))	!!}
                    </div>

                    <div class="col-sm-3">
                        {!!	Form::text('boeking[0][totaal]', null, array('placeholder' => 'Subtotaal','class' => 'form-control'))	!!}
                    </div>

                    <div class="col-sm-1">
                        <div class="control-group"><a class="fieldset_verwijderen btn btn-danger glyphicon glyphicon-trash"></a>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="Bestand BestandTemplate" style="display: none;" disabled>
                    <div class="col-sm-11">
                        {!!   Form::file('bestand[0][data]', old('bijlage', array('class' => 'btn'))) !!}
                    </div>
                    <div class="col-sm-1">
                        <div class="control-group"><a class="fieldset_verwijderen btn btn-danger glyphicon glyphicon-trash"></a>
                        </div>
                    </div>
                </fieldset>
            </div>
            {!! Form::close() !!}

        </div>
        <style>
            .Boeking {
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #f5f5f5;
                border: 1px solid #e3e3e3;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
                box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            }

            .Bestand {
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #f5f5f5;
                border: 1px solid #e3e3e3;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
                box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function () {

                $('#factuurdatum').datepicker({
                    format: "yyyy-mm-dd",
                    language: "nl",
                    calendarWeeks: true,
                    autoclose: true,
                    todayHighlight: true
                });

                $('#betaaldatum').datepicker({
                    format: "yyyy-mm-dd",
                    language: "nl",
                    calendarWeeks: true,
                    autoclose: true,
                    todayHighlight: true
                });

            });
        </script>

        <script type="text/javascript">
            (function (window, document, $, undefined) {
                var get_total = function (e) {
                    var $total = 0;
                    $('.Boekings').find('input[name*="totaal"]').each(function (k, e) {
                        if (!$(this).val() == "") {
                            $total += parseFloat($(this).val());
                        }
                    });
                    return $total;
                };
                var set_total = function (e) {
                    $("label[for='totaal_sum']").text(get_total);
                };

                var fieldset_verwijderen = function (e) {
                    $(this).closest('fieldset').remove();
                };

                var boeking_toevoegen = function (e) {
                    var count = $('.Boeking').length;
                    var $template = $('.BoekingTemplate').clone(true);
                    $template.removeClass('BoekingTemplate').prop("disabled", false).show();
                    $template.find('select, input').each(function (k, e) {
                        $(this).attr("name", $(this).attr("name").replace('0', count));
                    });
                    $('.Boekings').append($template);
                };

                var btw_toevoegen = function (e) {
                    var count = $('.Boeking').length;
                    var $template = $('.BoekingTemplate').clone(true);
                    $template.removeClass('BoekingTemplate').prop("disabled", false).show();
                    $template.find('select, input').each(function (k, e) {
                        $(this).attr("name", $(this).attr("name").replace('0', count));
                    });

                    var btw = 0.06 ;
                    var $total = 0;
                    $('.Boekings').find('input[name*="totaal"]').each(function (k, e) {
                        if (!$(this).val() == "") {
                            $total += parseFloat($(this).val());
                        }
                    });
                    $btwtotaal = $total * btw;

                    $template.find('[name$="[grootboekrekening_id]"]').val(37).prop('selected', true);
                    $template.find('[name$="[beschrijving]"]').val('21% btw (voorheffing)');
                    $template.find('[name$="[totaal]"]').val($btwtotaal);
                    $('.Boekings').append($template);
                };

                var km_toevoegen = function (e) {
                    var count = $('.Boeking').length;
                    var $template = $('.BoekingTemplate').clone(true);
                    $template.removeClass('BoekingTemplate').prop("disabled", false).show();
                    $template.find('select, input').each(function (k, e) {
                        $(this).attr("name", $(this).attr("name").replace('0', count));
                    });

                    var afstand_tot_relatie  = $('.Relatie').find('option:selected').data('afstand');
                    if(!afstand_tot_relatie) {
                        alert('Let op , Relatie niet gekozen, relatie afstand niet ingevoerd of 0 km');
                        return false;
                    }
                    var vergoeding_per_km = 0.19 ;
                    $kmvergoedingtotaal = 2 * (afstand_tot_relatie * vergoeding_per_km);

                    $template.find('[name$="[grootboekrekening_id]"]').val(34).prop('selected', true);
                    $template.find('[name$="[beschrijving]"]').val('km vergoeding - retour '+$('.Relatie').find('option:selected').text()+' | 2 x ' + afstand_tot_relatie.toString() + ' km a 0.19 ');
                    $template.find('[name$="[totaal]"]').val($kmvergoedingtotaal);
                    $('.Boekings').append($template);
                };

                var bestand_toevoegen = function (e) {
                    var count = $('.Bestand').length;
                    var $template = $('.BestandTemplate').clone(true);
                    $template.removeClass('BestandTemplate').prop("disabled", false).show();
                    $template.find('select, input').each(function (k, e) {
                        $(this).attr("name", $(this).attr("name").replace('0', count));
                    });
                    $('.Bestanden').append($template);
                };

                $(function () {
                    set_total();
                    $('.km_toevoegen').on('click', km_toevoegen);
                    $('.btw_toevoegen').on('click', btw_toevoegen);
                    $('.boeking_toevoegen').on('click', boeking_toevoegen);
                    $('.bestand_toevoegen').on('click', bestand_toevoegen);
                    $('.fieldset_verwijderen').on('click', fieldset_verwijderen);
                    $('.BoekingTemplate').find('input[name*="totaal"]').on('change mouseenter mouseleave', set_total);
                });
            }(window, document, jQuery));
        </script>
    </div>
@stop

