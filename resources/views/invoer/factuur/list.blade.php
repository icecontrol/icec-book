@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="hero-unit">
            <div class="title"><h1></h1></div>
            <div class="hero-unit">
                <div class="row-fluid">
                    <div class="panel-heading"><h1>Facturen <a href="{{url()->current().'/add'}}"><span class=" btn btn-success glyphicon glyphicon-plus" style="float: right"></span></a></h1></div>
                    <hr>
                </div>
                <br>
                <table id="FactuurList" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <th class="col-sm-1">Datum</th>
                        <th class="col-sm-2">Relatie</th>
                        <th class="col-sm-1">Ref</th>
                        <th class="col-sm-1">Factuurtotaal</th>
                        <th class="col-sm-1">Bijlage(s)</th>
                        <th class="col-sm-1">Action</th>
                    </thead>
                    <tbody>
                        @foreach($facturen as $factuur)

                        <tr>
                            <td>{!!$factuur->factuurdatum  !!}</td>
                            <td>{!!$factuur->Relatie->bedrijf  !!}</td>
                            <td>{!!$factuur->factuurref  !!}</td>
                            <td>{!!$factuur->Valuta->symbool  !!} {!! $factuur->getFactuurBoekingsTotaal()  !!}</td>
                            <td>
                                @if(count($factuur->bijlages) > 0)
                                    <a href="" class="glyphicon glyphicon-ok fa-2x "></a>
                                @endif
                            </td>
                           <td>
                                <a class="btn btn-success" href="/invoer/factuur/view/{!! $factuur->id !!}">View</a>
                                <a class="btn btn-warning" href="/invoer/factuur/edit/{!! $factuur->id !!}">Edit</a>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('#FactuurList').dataTable({
                "order": [[ 0, "desc" ]],
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "bSortable": false },
                ]
            });
        });
    </script>
@stop

