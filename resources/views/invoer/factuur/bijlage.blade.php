@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="hero-unit">
            <div class="title"><h1></h1></div>
            <div class="hero-unit">
                <div class="row-fluid">
                    <div class="panel-heading"><h1>Bijlage : {!! $bijlage->naam !!}  </h1></div>
                    <hr>

                    <div class='control-group '></div>
                    <embed src="data:{{$bijlage->mimetype}};base64,{{$bijlage->data}}" width="100%"
                            @if($bijlage->mimetype == 'application/pdf')
                            height="780px"
                            @else
                            height="auto"
                            @endif
                    ></embed>
                </div>
                </div>
                <br>

            </div>
        </div>
    </div>
@stop

