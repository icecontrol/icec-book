@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="hero-unit">
            <div class="title"><h1></h1></div>
            <div class="hero-unit">
                <div class="row-fluid">
                    <div class="panel-heading"><h1>Factuur # {!! $factuur->id !!} View </h1></div>
                    <hr>

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        @include('partials.factuur.infoview')
                </div>
                <br>
            </div>
        </div>
    </div>
@stop

