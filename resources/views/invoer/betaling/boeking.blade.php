@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="content">
            <div class="hero-unit">
                <div class="title"><h1></h1></div>

                <div class="hero-unit">
                    <div class="row-fluid">
                        <div class="panel-heading"><h1>Betaling invoer </h1></div>
                        <hr>

                        @include('partials.validationerrors')


                        <div class="container">
                            <div class="row">
                                @include('partials.factuur.infoview')
                            </div>
                                <div class="container">
                                    @include('partials.betalingen.betalinginvoer')
                                </div>
                            {{--</div>--}}
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function () {
                                $('#factuurdatum').datepicker({
                                    format: "yyyy-mm-dd",
                                    language: "nl",
                                    calendarWeeks: true,
                                    autoclose: true,
                                    todayHighlight: true
                                });

                                $('#betaaldatum').datepicker({
                                    format: "yyyy-mm-dd",
                                    language: "nl",
                                    calendarWeeks: true,
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            });
                        </script>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
