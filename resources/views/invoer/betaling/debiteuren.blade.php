@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="content">
            <div class="hero-unit">
                <div class="title"><h1></h1></div>

                <div class="hero-unit">
                    <div class="row-fluid">
                        <div class="panel-heading"><h1>Betaling invoer </h1></div>
                        <hr>

                        @include('partials.validationerrors')

                        <div class="container">
                            <h3>Soort betaling</h3>
                            <ul class="nav nav-pills nav-justified">
                                <li><a href="/invoer/betaling/Crediteuren">Crediteuren</a></li>
                                <li class="active"><a href="/invoer/betaling/Debiteuren">Debiteuren</a></li>
                                {{--<li><a href="/invoer/betaling/Kuispost">Kuispost</a></li>--}}
                                {{--<li><a href="/invoer/betaling/Vermogen">Vermogen</a></li>--}}
                            </ul>
                        </div>

                        <div class="container">
                            <div class="row">

                                <div class="container">
                                    @include('partials.betalingen.factuurs_table_list')
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function () {
                                $('#factuurdatum').datepicker({
                                    format: "yyyy-mm-dd",
                                    language: "nl",
                                    calendarWeeks: true,
                                    autoclose: true,
                                    todayHighlight: true
                                });

                                $('#betaaldatum').datepicker({
                                    format: "yyyy-mm-dd",
                                    language: "nl",
                                    calendarWeeks: true,
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            });
                        </script>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
