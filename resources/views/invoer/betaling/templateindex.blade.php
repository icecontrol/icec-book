@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')

        <div class="content">
            <div class="hero-unit">
                <div class="title"><h1></h1></div>

                <div class="hero-unit">
                    <div class="row-fluid">
                        <div class="panel-heading"><h1>Betaling invoer </h1></div>
                        <hr>

                  @include('partials.validationerrors')

                        @if($stap == 0)

                            <div class="container">
                                <h3>Soort betaling</h3>
                                <ul class="nav nav-pills nav-justified">
                                    <li><a href="/invoer/betaling/Crediteuren">Crediteuren</a></li>
                                    <li><a href="/invoer/betaling/Debiteuren">Debiteuren</a></li>
                                    <li><a href="/invoer/betaling/Kuispost">Kuispost</a></li>
                                    <li><a href="/invoer/betaling/Vermogen">Vermogen</a></li>
                                </ul>
                            </div>

                        @elseif($stap ==1)
                            <div class="container">
                                <h3>Soort betaling</h3>
                                <ul class="nav nav-pills nav-justified">
                                    @if(strcasecmp($soort, 'Crediteuren') == 0)
                                        <li class="active"><a href="/betaling/Crediteuren">Crediteuren</a></li>
                                    @else
                                        <li><a href="/betaling/Crediteuren">Crediteuren</a></li>
                                    @endif
                                    @if(strcasecmp($soort, 'Debiteuren') == 0)
                                        <li class="active"><a href="/betaling/Debiteuren">Debiteuren</a></li>
                                    @else
                                        <li><a href="/betaling/Debiteuren">Debiteuren</a></li>
                                    @endif
                                    @if(strcasecmp($soort, 'Kuispost') == 0)
                                        <li class="active"><a href="/betaling/Kuispost">Kuispost</a></li>
                                    @else
                                        <li><a href="/betaling/Kuispost">Kuispost</a></li>
                                    @endif
                                    @if(strcasecmp($soort, 'Vermogen') == 0)
                                        <li class="active"><a href="/betaling/Vermogen">Vermogen</a></li>
                                    @else
                                        <li><a href="/betaling/Vermogen">Vermogen</a></li>
                                    @endif

                                </ul>
                            </div>

                            <div class="container">
                                <div class="row">

                                    <div class="container">
                                        <div class="control-group">
                                            <h3>Factuur</h3>

                                            <div class="row-fluid">
                                                @if(isset($facturen))
                                                    <table class="table table-hover">
                                                        <tr style=>
                                                            <td><b>Id</b></td>
                                                            {{--<td><b>Referentie</b></td>--}}
                                                            <td><b>Factuur datum</b></td>
                                                            <td><b>Relatie</b></td>
                                                            <td><b>Journaal</b></td>
                                                            <td><b>Valuta</b></td>
                                                            <td><b>Factuur totaal</b></td>
                                                            <td><b>Openstaand</b></td>
                                                        </tr>

                                                        @foreach ($facturen as $factuur)
                                                            <tr>
                                                                <td><a href="/betaling/{{$soort}}/{{ $factuur->id }}">{{ $factuur->id }}</a></td>
                                                                {{--<td>{{ $factuur->factuurref }}</td>--}}
                                                                <td>{{ $factuur->factuurdatum  }}</td>
                                                                <td>{{ $factuur->relatie  }}</td>
                                                                <td>{{ $factuur->hoofdjournaal  }} - {{ $factuur->journaal  }}</td>
                                                                <td>{{ $factuur->valuta }}</td>
                                                                <td>{{ $factuur->ftotincl }}</td>
                                                                <td>{{ $factuur->openstaand }}</td>
                                                            </tr>

                                                        @endforeach

                                                    </table>
                                                @else
                                                    <div class="container">
                                                        <h4>Geen open facturen</h4>
                                                    </div>

                                                @endif


                                                <hr>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        @elseif($stap ==2)
                            <div class="container">
                                <h3>Soort betaling</h3>
                                <ul class="nav nav-pills nav-justified">
                                    {{--<li class="active"><a href="#">Home</a></li>--}}
                                    @if(strcasecmp($soort, 'Crediteuren') == 0)
                                        <li class="active"><a href="/betaling/Crediteuren">Crediteuren</a></li>
                                    @else
                                        <li><a href="/betaling/Crediteuren">Crediteuren</a></li>
                                    @endif
                                    @if(strcasecmp($soort, 'Debiteuren') == 0)
                                        <li class="active"><a href="/betaling/Debiteuren">Debiteuren</a></li>
                                    @else
                                        <li><a href="/betaling/Debiteuren">Debiteuren</a></li>
                                    @endif
                                    @if(strcasecmp($soort, 'Kuispost') == 0)
                                        <li class="active"><a href="/betaling/Kuispost">Kuispost</a></li>
                                    @else
                                        <li><a href="/betaling/Kuispost">Kuispost</a></li>
                                    @endif
                                    @if(strcasecmp($soort, 'Vermogen') == 0)
                                        <li class="active"><a href="/betaling/Vermogen">Vermogen</a></li>
                                    @else
                                        <li><a href="/betaling/Vermogen">Vermogen</a></li>
                                    @endif
                                </ul>
                            </div>

                            <div class="container">
                                <div class="row">

                                    <div class="container">
                                        <div class="control-group">
                                            <h3>Factuur</h3>

                                            <div class="row-fluid">
                                                @if(isset($facturen))
                                                    <table class="table table-hover">
                                                        <tr style=>
                                                            <td><b>Id</b></td>
                                                            {{--<td><b>Referentie</b></td>--}}
                                                            <td><b>Factuur datum</b></td>
                                                            <td><b>Relatie</b></td>
                                                            <td><b>Journaal</b></td>
                                                            <td><b>Valuta</b></td>
                                                            <td><b>Factuur totaal</b></td>
                                                            <td><b>Openstaand</b></td>
                                                        </tr>

                                                        @foreach ($facturen as $factuur)
                                                            @if(strcasecmp($id, $factuur->id) == 0)
                                                                <tr class="info">
                                                            @else
                                                                <tr>
                                                                    @endif


                                                                    <td><a href="/betaling/{{$soort}}/{{ $factuur->id }}">{{ $factuur->id }}</a></td>
                                                                    {{--<td>{{ $factuur->factuurref }}</td>--}}
                                                                    <td>{{ $factuur->factuurdatum  }}</td>
                                                                    <td>{{ $factuur->relatie  }}</td>
                                                                    <td>{{ $factuur->hoofdjournaal  }} - {{ $factuur->journaal  }}</td>
                                                                    <td>{{ $factuur->valuta }}</td>
                                                                    <td>{{ $factuur->ftotincl }}</td>
                                                                    <td>{{ $factuur->openstaand }}</td>
                                                                </tr>

                                                                @endforeach

                                                    </table>
                                                @else
                                                    <div class="container">
                                                        <h4>Geen open facturen</h4>
                                                    </div>

                                                @endif

                                            </div>
                                            <hr>
                                            {!! Form::open() !!}
                                            <div class="row">

                                                <div class="row">
                                                    <div class="col-sm-offset-1 col-sm-2">
                                                        {!!	Form::label('Geselecteerde factuur')	!!}
                                                    </div>
                                                    <div class="col-sm-1">
                                                        # {{$id}}
                                                    </div>

                                                    <input type="hidden" name="factuur_id" id="factuur_id" value="{{$openfactuur->id}}">
                                                    <input type="hidden" name="factuurref" id="factuurref" value="{{$openfactuur->factuurref}}">


                                                    <div class="col-sm-offset-3 col-sm-3">
                                                        <a href="/inzien/{{$id}}" class="btn btn-warning">Factuur inzien</a>
                                                    </div>
                                                </div>

                                                <br>

                                                <div class="row">
                                                    <div class="col-sm-offset-1 col-sm-2">
                                                        <div class="control-group">
                                                            {!!	Form::label('betaaldatum','Overboekings datum')	!!}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-actions">
                                                            {!!	Form::input('date', 'betaaldatum', old('betaaldatum'))	!!}
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>

                                                <div class="row">
                                                    <div class="col-sm-offset-1 col-sm-2">
                                                        {!!	Form::label('liquide_middel','Liquide middel')	!!}<br>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select id="liquide_middel" name="liquide_middel">
                                                            <option value="" disabled selected>Kies Liquide middel</option>
                                                            @foreach($liquidemiddelen as $journaal)
                                                                <option value="{{$journaal['naam']}}">{{$journaal['naam']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <br>

                                                <div class="row">
                                                    <div class="col-sm-offset-1 col-sm-2">
                                                        {!!	Form::label('totincl','Overboeking Totaal ')	!!}
                                                    </div>
                                                    <div class="col-sm-1">
                                                        {!!	$openfactuur->valuta	!!}
                                                    </div>
                                                    <div class="col-sm-3">
                                                        {!!	Form::text('totincl', $openfactuur->openstaand,old('totincl'))	!!}
                                                    </div>

                                                </div>

                                                @if($openfactuur->valuta != 'EUR')
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-sm-offset-1 col-sm-2">
                                                            {!!	Form::label('toteur','Overboeking Totaal ')	!!}
                                                        </div>
                                                        <div class="col-xs-1">
                                                            EUR
                                                        </div>
                                                        <div class="col-sm-3">
                                                            {!!	Form::text('toteur')	!!}
                                                        </div>

                                                    </div>
                                                @endif

                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-offset-2 col-sm-8">
                                                        <button type="submit" class="btn btn-primary btn-lg btn-block" name="Invoeren" value="Invoeren" onclick="return confirm('Factuur invoeren ?');">
                                                            Invoeren
                                                        </button>
                                                    </div>
                                                </div>

                                                {!! Form::close() !!}


                                                <br>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        @elseif($stap ==3)
                            <div class="container">

                                <h3>Soort betaling</h3>
                                <ul class="nav nav-pills nav-justified">
                                    @if(strcasecmp($soort, 'Crediteuren') == 0)
                                        <li class="active"><a href="/betaling/Crediteuren">Crediteuren</a></li>
                                    @else
                                        <li><a href="/betaling/Crediteuren">Crediteuren</a></li>
                                    @endif
                                    @if(strcasecmp($soort, 'Debiteuren') == 0)
                                        <li class="active"><a href="/betaling/Debiteuren">Debiteuren</a></li>
                                    @else
                                        <li><a href="/betaling/Debiteuren">Debiteuren</a></li>
                                    @endif
                                    @if(strcasecmp($soort, 'Kuispost') == 0)
                                        <li class="active"><a href="/betaling/Kuispost">Kruispost</a></li>
                                    @else
                                        <li><a href="/betaling/Kuispost">Kruispost</a></li>
                                    @endif
                                    @if(strcasecmp($soort, 'Vermogen') == 0)
                                        <li class="active"><a href="/betaling/Vermogen">Vermogen</a></li>
                                    @else
                                        <li><a href="/betaling/Vermogen">Vermogen</a></li>
                                    @endif
                                </ul>


                            </div>
                            {!! Form::open() !!}
                            <br>


                            <br>
                            <div class="row">
                                <div class="col-md-2 col-md-offset-1">
                                    <div class="control-group">
                                        {!!	Form::label('debet','Debet')	!!}<br>
                                        <select id="debet" name="debet">
                                            <option value="" disabled selected>Kies debet rekening</option>
                                            @foreach($liquidemiddelen as $journaal)
                                                <option value="{{$journaal['naam']}}">{{$journaal['naam']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="control-group">
                                        <h1>&#8674</h1>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="control-group">
                                        <h2>Kruispost</h2>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="control-group">
                                        <h1>&#8674</h1>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="control-group">
                                        {!!	Form::label('credit','Credit')	!!}<br>
                                        <select id="credit" name="credit">
                                            <option value="" disabled selected>Kies credit rekening</option>
                                            @foreach($liquidemiddelen as $journaal)
                                                <option value="{{$journaal['naam']}}">{{$journaal['naam']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <br>
                            </br>


                            <div class="row">

                                <div class="col-sm-3">
                                    <div class="control-group">
                                        {!!	Form::label('betaaldatum','Overboekings datum')	!!}
                                        {!!	Form::input('date', 'betaaldatum', old('geboortedatum'))	!!}
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="control-group">
                                        {!!	Form::label('totincl','Overboeking Totaal ')	!!}
                                        {!!	Form::text('totincl', old('totincl'))	!!}
                                    </div>
                                </div>
                            </div>


                            <hr>
                            <div class="row">
                                <div class="col-sm-offset-2 col-sm-8">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block" name="Invoeren" value="Invoeren" onclick="return confirm('Factuur invoeren ?');">
                                        Invoeren
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                    </div>

                    @elseif($stap ==4)
                        <div class="container">
                            <h3>Soort betaling</h3>
                            <ul class="nav nav-pills nav-justified">
                                @if(strcasecmp($soort, 'Crediteuren') == 0)
                                    <li class="active"><a href="/betaling/Crediteuren">Crediteuren</a></li>
                                @else
                                    <li><a href="/betaling/Crediteuren">Crediteuren</a></li>
                                @endif
                                @if(strcasecmp($soort, 'Debiteuren') == 0)
                                    <li class="active"><a href="/betaling/Debiteuren">Debiteuren</a></li>
                                @else
                                    <li><a href="/betaling/Debiteuren">Debiteuren</a></li>
                                @endif
                                @if(strcasecmp($soort, 'Kuispost') == 0)
                                    <li class="active"><a href="/betaling/Kuispost">Kuispost</a></li>
                                @else
                                    <li><a href="/betaling/Kuispost">Kuispost</a></li>
                                @endif
                                @if(strcasecmp($soort, 'Vermogen') == 0)
                                    <li class="active"><a href="/betaling/Vermogen">Vermogen</a></li>
                                @else
                                    <li><a href="/betaling/Vermogen">Vermogen</a></li>
                                @endif

                            </ul>
                        </div>

                        <div class="container">
                            <div class="row">

                                <div class="container">
                                    <div class="control-group">
                                        <h3>Vermogen invoer</h3>


                                    </div>
                                </div>
                            </div>
                        </div>



                    @endif

                    <script type="text/javascript">
                        $(document).ready(function () {

                            $('#factuurdatum').datepicker({
                                format: "yyyy-mm-dd",
                                language: "nl",
                                calendarWeeks: true,
                                autoclose: true,
                                todayHighlight: true
                            });

                            $('#betaaldatum').datepicker({
                                format: "yyyy-mm-dd",
                                language: "nl",
                                calendarWeeks: true,
                                autoclose: true,
                                todayHighlight: true
                            });

                        });
                    </script>

                </div>
            </div>
        </div>
    </div>
    </div>

@stop
