@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        {!! Form::open() !!}
        <br>
        <br>
        <div class="row">
            <div class="col-xs-6">
                <blockquote>
                    <h1 class="featurette-heading">Prive Boekhouder</h1>
                    <small>by I.C.E.C - <cite title="Source Title">ICE Control &copy; {{date('Y')}}</cite></small>
                    <p>OpenSource boekhouding voor Zzp'ers</p>
                    <div style="text-align: center;  overflow-x: hidden; margin-top: 5%">
                        {!! Html::image('/image/new_logo.png', null, array('class' => 'thumbnail')) !!}
                    </div>
                </blockquote>
            </div>
        </div>
        <hr>
        <br>
        @if(!Auth::check())
        <div class="row">
            @include('partials.validationerrors')
            <div class="col-xs-offset-3 col-xs-6">
                <div class="row form-group">
                    <div class="row">
                        <div class="input-group margin-bottom-sm">
                            <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                            {!!	Form::text('email', old('email'), array('class' =>'form-control', 'placeholder' => ' Email address'))	!!}
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                            {!!	Form::password('password',array('class' =>'form-control', 'placeholder' => ' Wachtwoord'))	!!}
                        </div>
                        <br>
                        <div class="control-group">
                            <div class="btn btn-group-lg btn-block">
                                <button type="submit" class="btn btn-primary" name="checkfa">
                                    Login
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    </div>
    {!! Form::close() !!}
@stop




