@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="content">
            <div class="hero-unit">
                <div class="title"><h1></h1></div>
                {!! Form::open() !!}
                <div class="hero-unit">
                    <h1>Overzichten</h1>
                    <br>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-lg btn-info ">
                                    <input name="jaar" value="2012" type="radio">2012
                                </label>
                                <label class="btn btn-lg btn-info">
                                    <input name="jaar" value="2013" type="radio">2013
                                </label>
                                <label class="btn  btn-lg btn-info">
                                    <input name="jaar" value="2014" type="radio">2014
                                </label>
                                <label class="btn btn-lg btn-info">
                                    <input name="jaar" value="2015" type="radio">2015
                                </label>
                                <label class="btn btn-lg btn-info">
                                    <input name="jaar" value="2016" type="radio">2016
                                </label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-lg btn-warning ">
                                    <input name="periode" value="1" type="radio">Q1
                                </label>
                                <label class="btn btn-lg btn-warning">
                                    <input name="periode" value="2" type="radio">Q2
                                </label>
                                <label class="btn  btn-lg btn-warning">
                                    <input name="periode" value="3" type="radio">Q3
                                </label>
                                <label class="btn btn-lg btn-warning">
                                    <input name="periode" value="4" type="radio">Q4
                                </label>
                                <label class="btn btn-lg btn-warning">
                                    <input name="periode" value="5" type="radio">Q1-Q5
                                </label>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-lg-3">
                            <select name="type" class="form-control ">
                                <option value="Resultaat">Resultatenrekening</option>
                                <option value="Balans">Balansrekening</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-3">
                            {!! Form::submit('Toon', array('class'=> 'btn btn-lg btn-primary')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

    {!! Form::close() !!}


    <script>
        (function (window, document, $, undefined) {

            var overzicht_genereren = function (e) {
                console.log('asd');
            };

            $(function () {

                $('#overzicht_genereren > button').on('click', overzicht_genereren);

//                $("form").submit(function (e) {
//                    $(this).find('.locatieInhoud').each(function (i, e) {
//                        $(this).removeAttr('disabled');
//                    });
//                    $(this).find('.Locatie').each(function (i, e) {
//                        $(this).find('input[name*=positie]').val(i);
//                    });
//                });
            });
        }(window, document, jQuery));

    </script>

@stop
