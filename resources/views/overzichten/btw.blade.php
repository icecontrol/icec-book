@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="content">
            <div class="hero-unit">
                <div class="title"><h1></h1></div>
                {!! Form::open() !!}
                <div class="hero-unit">
                    @include('partials.date-range-pick')

                    <br>


                    <div class="row">

                        <div>
                            @if(isset($btw))

                                <div class="row-fluid">

                                    <table class="table table-hover">
                                        <tr style=>
                                            <td><b>Referentie</b></td>
                                            <td><b>Relatie</b></td>
                                            <td><b>Factuur datum</b></td>

                                            <td><b>btw in €</b></td>
                                        </tr>

                                        @foreach ($btw as $factuur)
                                            <tr>
                                                <td><a href="/inzien/{{ $factuur->factuur_id }}">{{ $factuur->factuurref }}</a></td>
                                                <td>{{ $factuur->relatie  }}</td>
                                                <td>{{ $factuur->factuurdatum  }}</td>

                                                <td>
                                                    @if(strcasecmp($factuur->in_of_uit, 'uit') == 0)
                                                        - {{ $factuur->btw }}
                                                    @elseif(strcasecmp($factuur->in_of_uit, 'in') == 0)
                                                        + {{ $factuur->btw }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach

                                    </table>
                                    <hr>

                                    <div class="col-md-offset-9 col-md-2">
                                        <strong> Totaal {{$totaal}}  </strong><br>
                                    </div>
                                    @endif
                                    <br></br>
                                    <br></br>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

    {!! Form::close() !!}

@stop
