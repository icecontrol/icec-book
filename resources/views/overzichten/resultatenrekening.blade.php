@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="content">
            <div class="hero-unit">
                <br>
                <div class="title"><h1><blockquotes>Resultaten Rekening | <strong><?php echo $jaar; ?> <?php echo $periode; ?></strong></blockquotes></h1></div>

                <div class="hero-unit">
                    <br>
                    @include('partials.overzichten.resultatenoverzicht')
                    @include('partials.overzichten.rubriek')
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
