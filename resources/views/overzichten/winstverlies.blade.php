@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="content">
            <div class="hero-unit">
                <div class="title"><h1></h1></div>

                {!! Form::open() !!}
                <div class="hero-unit">

                    @include('partials.date-range-pick')

                    <br>

                    <div class="row">

                        <div>
                            @if(isset($kwartaal))
                                <hr>
                                <div class="row">


                                </div>
                                <div class="row ">

                                    <div class="col-md-6 ">
                                        <h3>Activa</h3>

                                        <div class="row">
                                            <div class="col-xs-offset-1 col-xs-11">
                                                <b>Vaste activia</b>
                                                @foreach($vasteactivias as $vasteac)
                                                    <div class="row">
                                                        <div class="col-xs-offset-1 col-xs-5">
                                                            <h5>{{$vasteac->naam}}</h5>
                                                        </div>
                                                        <div class="col-xs-offset-1 col-xs-5">
                                                            <h5>{{$vaste[$vasteac->naam]}}</h5>
                                                        </div>

                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <br>


                                        <div class="row">
                                            <div class="col-xs-offset-1 col-xs-11">
                                                <b>Vlottende activia</b>

                                                @foreach($vlottendeactivias as $vlot)
                                                    <div class="row">
                                                        <div class="col-xs-offset-1 col-xs-10">
                                                            <div class="form-group">
                                                                <button class="btn " value="{{$vlot->naam}}"><b>{{$vlot->naam}}</b></button>
                                                            </div>
                                                        </div>

                                                        @foreach($vlottende[$vlot->naam] as $vlo)
                                                            <div class="col-xs-offset-2 col-xs-4">
                                                                <button class="btn " value="{{$vlo['naam']}}">{{$vlo['naam']}}</button>
                                                            </div>
                                                            <div class="col-xs-offset-1 col-xs-5">
                                                                <h5>{{$vlo['totaal']}}</h5>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <br>
                                                @endforeach

                                                    <div class="row">
                                                        <div class="col-xs-offset-1 col-xs-5">
                                                            <h5>kmvergoeding</h5>
                                                        </div>
                                                        <div class="col-xs-offset-1 col-xs-5">
                                                            <h5>{{$kmvergoeding}}</h5>
                                                        </div>
                                                    </div>

                                                    <br>



                                            </div>



                                        </div>
                                    </div>
                                    <br>


                                    {{--</div>--}}

                                    <div class="col-xs-6">
                                        <h3>Passiva</h3>

                                        <div class="row">
                                            <div class="col-xs-offset-1 col-xs-11">
                                                <b>Omzet</b>
                                                @foreach($omzet as $vaste)
                                                    <div class="row">
                                                        <div class="col-xs-offset-1 col-xs-5">
                                                            <h5>{{$vaste->naam}}</h5>
                                                        </div>
                                                        <div class="col-xs-offset-1 col-xs-5">
                                                            <h5>{{$omz[$vaste->naam]}}</h5>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <br>

                                    </div>


                                    @endif
                                    <br></br>
                                    <br></br>

                                </div>
                                <hr>
                                <div class="col-md-offset-7 col-md-4">
                                    @if(isset($totaal))
                                        <strong>
                                            @if($totaal < 0)
                                                <h3>Verlies {{$totaal}}</h3>
                                            @else
                                                <h3>Winst {{$totaal}}</h3>
                                            @endif
                                        </strong><br>
                                    @endif
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

    {!! Form::close() !!}

    <script>
        (function (window, document, $, undefined) {

            var nieuwe_template = function () {
                var locatietemplate = $('div#content').find('.Locaties > span[data-template]').data('template');
                var count = $('.Locaties > fieldset').length;
                locatietemplate = $(locatietemplate.replace(/__index__/g, count));
                locatietemplate.find('input[name*=positie]').val(count);

                $.post("/ajax/template/getFieldset", {type: $(this).val(), basecomponent: $('#Template').find('input[name=basecomponenttype]').val()}, function (data) {
                    var $fieldset = $(data.fieldset);
                    $fieldset.find('input, textarea, select').each(function (i, e) {
                        $(this).attr("name", 'locaties[' + count + '][locatieInhoud][' + $(this).attr('name') + ']');
                    });
                    locatietemplate.append($fieldset);
                    $('.Locaties > span[data-template]').before(locatietemplate);
                    set_listners(locatietemplate);
                });
            };

            var mogelijkheid_toevoegen = function () {
                var mogelijkheid = $(this).closest('fieldset').find('span[data-template]').data('template');
                var locatiecount = $('.Locaties > fieldset').index($(this).closest('.Locatie'));
                var count = $(this).closest('fieldset').find('.Mogelijkheden  > fieldset').length;
                var $mogelijkheden = $(this).closest('fieldset').find('.Mogelijkheden');

                if ($(this).closest('fieldset').find('input[name*="[created]"]').val() == 1) {
                    mogelijkheid = $(mogelijkheid);
                    mogelijkheid.find('input, textarea, select').each(function (i, e) {
                        $(this).attr("name", 'locaties[' + locatiecount + '][locatieInhoud][mogelijkheden][' + count + '][' + $(this).attr('name') + ']');
                    });
                } else { mogelijkheid = $(mogelijkheid.replace(/__index__/g, count));};

                $mogelijkheden.append(mogelijkheid);
                set_listners($mogelijkheden);
            };

            var set_listners = function (container) {
                container.find('.move_up').on('click', move_up);
                container.find('.move_down').on('click', move_down);
                container.find('.mogelijkheid_verwijderen').on('click', mogelijkheid_verwijder);
                container.find('.mogelijkheid_toevoegen').on('click', mogelijkheid_toevoegen);
                container.find('.locatie_verwijderen').on('click', locatie_verwijder);
            };


            var move_down = function (e) {
                var locatie = $(this).closest('.Locatie');
                console.log('trigger');
                if (locatie.next().is('.Locatie'))
                    locatie.next().after(locatie);
            };

            // disable all inputs on existing locatie to prevent altering without cloning
            var disable_all_existing_fields = function (e) {
                $('.Locatie').each(function (i, e) {
                    // disable locatieinhoud for editing and add edit button
                    $(this).find('.locatieInhoud').attr('disabled', true);
                    //add edit button and set its listner
                    $(this).append($('<button class="edit_locatie btn btn-warning" type="button">Aanpassen <span class="glyphicon glyphicon-edit"></span></button>'));
                    $(this).find('.edit_locatie').on('click', edit_locatie);
                    // tag locatie so we know it was already there
                    $(this).append($('<input type="hidden" class="bestaande" value="1">'));
                });
            };

            //make locatie editable ,also remove its id so we always keep the old version
            var edit_locatie = function (e) {
                $(this).closest('.Locatie').find('.locatieInhoud').attr('disabled', false);
                $(this).closest('.Locatie').find('.locatieInhoud').find('input[name*="[id]"]').remove();
            };

            $(function () {
                // disable all inputs on existing locatie to prevent altering without cloning
                disable_all_existing_fields();

                $('#bestaande_vraag > button').on('click', bestaande_vraag);


                $("form").submit(function (e) {
                    $(this).find('.locatieInhoud').each(function (i, e) {
                        $(this).removeAttr('disabled');
                    });
                    $(this).find('.Locatie').each(function (i, e) {
                        $(this).find('input[name*=positie]').val(i);
                    });
                });
            });
        }(window, document, jQuery));

    </script>

@stop
