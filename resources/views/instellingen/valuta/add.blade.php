@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="row col-xs-12">
            {{ Form::open() }}
            <div class="row">
                <div class="col-xs-12">
                    <h1>Valuta toevoegen</h1>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-2">
                    {{ Form::label('naam', 'Naam Valuta') }}
                </div>
                <div class="col-xs-10">
                    {{ Form::text('naam',null, array('class' => 'form-control')) }}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-2">
                    {{ Form::label('symbool', 'Naam Symbool') }}
                </div>
                <div class="col-xs-10">
                    {{ Form::text('symbool',null, array('class' => 'form-control')) }}
                </div>
            </div>
            <br>
            {{ Form::submit('Invoeren',array('class' => 'btn btn-success btn-block')) }}
            {{ Form::close() }}
            <hr>
            <div class="row">
                @include('flash::message')
            </div>
        </div>
    </div>
@stop
