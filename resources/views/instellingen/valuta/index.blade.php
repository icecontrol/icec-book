@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="row-fluid col-xs-12">
            <div class="panel-heading"><h1>Valutas <a href="{{url()->current().'/add'}}"><span class=" btn btn-success glyphicon glyphicon-plus" style="float: right" ></span></a></h1></div>
            <hr>
        </div>
        <br>
        <div class="row col-xs-12">
            <table id="ValutasList" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="col-lg-10">Naam</th>
                        <th class="col-lg-10">Symbool</th>
                        <th class="col-lg-10">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($valutas as $valuta)
                        <tr>
                            <th class="col-lg-10">{{$valuta->naam}}</th>
                            <th class="col-lg-10">{{$valuta->symbool}}</th>
                            <th class="col-lg-1">
                                <a class="btn btn-xs btn-warning btnAdd" href="{{url()->current().'/edit/'.$valuta->id }}">
                                    <span class="glyphicon glyphicon-edit"></span>
                                    Edit
                                </a>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <hr>
        <div class="row col-xs-12">
            @include('flash::message')
        </div>
    </div>
    <script>
        $(function () {
            if($('#ValutasList').find('tbody tr').length > 1){
                $('#ValutasList').dataTable({
                    "order": [[0, "asc"]],
                    "columns": [
                        null,
                        null,
                        {"bSortable": false},
                    ]
                });
            }
        });
    </script>
@stop
