@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')

        <div class="row-fluid">
            <div class="panel-heading"><h1>Subrubriek <a href="{{url()->current().'/add'}}"><span class=" btn btn-success glyphicon glyphicon-plus" style="float: right" ></span></a></h1></div>
            <hr>
        </div>
        <br>
        <div class="row">
            <table id="SubrubriekList" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="col-lg-1">#</th>
                        <th class="col-lg-10">Naam</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($subrubrieks as $subrubriek)
                        <tr>
                            <th class="col-lg-1">{{$subrubriek->getFullSubrubriekNummer()}}</th>
                            <th class="col-lg-10">{{$subrubriek->naam}}</th>
                            <th class="col-lg-1">
                                <a class="btn btn-xs btn-warning btnAdd" href="{{url()->current().'/edit/'.$subrubriek->id }}">
                                    <span class="glyphicon glyphicon-edit"></span>
                                    Edit
                                </a>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <hr>
        <div class="row">
            @include('flash::message')
        </div>
    </div>
    <script>
        $(function () {
            $('#SubrubriekList').dataTable({
                "order": [[0, "asc"]],
                "columns": [
                    null,
                    null,
                    {"bSortable": false},
                ]
            });
        });
    </script>
@stop
