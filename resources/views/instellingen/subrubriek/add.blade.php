@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')

        <br>
        {{ Form::open() }}

        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('rubrieks_id', 'Valt onder') }}
            </div>
            <div class="col-xs-10">
                {{ Form::select('rubrieks_id', $rubrieks, null, ['class' => 'form-control']) }}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('sub_rubrieks_nummer', 'SubRubriek Nummer') }}
            </div>
            <div class="col-xs-2">
                {{ Form::number('sub_rubrieks_nummer',null, array('class' => 'form-control','min' => 0, 'max' => 9)) }}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('naam', 'Naam SubRubriek') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('naam',null, array('class' => 'form-control')) }}
            </div>
        </div>
        {{ Form::submit() }}
        {{ Form::close() }}

        <hr>

        <div class="row">
            @include('flash::message')
        </div>
    </div>
@stop
