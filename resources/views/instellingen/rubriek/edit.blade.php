@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <br>
         {{ Form::model($rubriek) }}
        <div class="row">

            <div class="col-xs-10">
                <h2>Rubriek {{$rubriek->rubrieks_nummer}}</h2>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('type', 'Type') }}
            </div>
            <div class="col-xs-2">
                {{ Form::select('type', ['Balans' => 'Balans', 'Resultaat' => 'Resultaat'], $rubriek->type, array('class' => 'form-control')) }}
                {{--{{ Form::text('type') }}--}}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('naam', 'Naam') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('naam',null, array('class' => 'form-control')) }}
            </div>
        </div>
        {{ Form::submit() }}
        {{ Form::close() }}
        <hr>
        <div class="row">
            @include('flash::message')
        </div>
    </div>
@stop
