@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')

        <br>
        <h1>Rubrieken</h1>

        <div class="row">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="col-lg-1">Rubriek</th>
                            <th class="col-lg-1">Type</th>
                            <th class="col-lg-10">Naam</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rubrieks as $rubriek)
                        <tr>
                            <th class="col-lg-1">{{$rubriek->rubrieks_nummer}}</th>
                            <th class="col-lg-1">{{$rubriek->type}}</th>
                            <th class="col-lg-9">{{$rubriek->naam}}</th>
                            <th class="col-lg-1">
                                <a class="btn btn-xs btn-warning btnAdd" href="{{url()->current().'/edit/'.$rubriek->id }}">
                                    <span class="glyphicon glyphicon-edit"></span>
                                     Edit
                                </a>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>

        <hr>

        <div class="row">
            @include('flash::message')
        </div>

    </div>
@stop
