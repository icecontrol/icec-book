@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')


        <div class="row-fluid">
            <div class="panel-heading"><h1>Grootboekrekeningen <a href="{{url()->current().'/add'}}"><span class=" btn btn-success glyphicon glyphicon-plus" style="float: right"></span></a></h1></div>
            <hr>
        </div>
        <br>
        <div class="row">
            <table id="GrootboekrekeningList" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="col-lg-1">#</th>
                        <th class="col-lg-1">Type</th>
                        <th class="col-lg-9">Naam</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($grootboekrekeningen as $grootboekrekening)
                        <tr>
                            <th class="col-lg-1">
                                {{$grootboekrekening->getFullGrootBoekNummer()}}
                            </th>
                            <th class="col-lg-1">
                                @if($grootboekrekening->type == 1)
                                    @if($grootboekrekening->subrubriek->rubriek->type == 'Balans')
                                        Activa
                                    @else
                                        Inkomst
                                    @endif
                                @else
                                    @if($grootboekrekening->subrubriek->rubriek->type == 'Balans')
                                        Pasiva
                                    @else
                                        Uitgave
                                    @endif
                                @endif
                            </th>
                            <th class="col-lg-9">{{$grootboekrekening->naam}}</th>
                            <th class="col-lg-1">
                                <a class="btn btn-xs btn-warning btnAdd" href="{{url()->current().'/edit/'.$grootboekrekening->id }}">
                                    <span class="glyphicon glyphicon-edit"></span>
                                    Edit
                                </a>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <hr>
        <div class="row">
            @include('flash::message')
        </div>
    </div>
    <script>
        $(function () {
            $('#GrootboekrekeningList').dataTable({
//                "order": [[0, "asc"]],
                "columns": [
                    null,
                    null,
                    null,
                    {"bSortable": false}
                ]
            });
        });
    </script>
@stop
