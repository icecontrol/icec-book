@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')

        <br>
        {{ Form::model($grootboekrekening) }}
        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('sub_rubrieks_id', 'Valt onder') }}
            </div>
            <div class="col-xs-10">
                {{ Form::select('sub_rubrieks_id', $subrubrieks, $grootboekrekening->sub_rubrieks_id, ['class' => 'form-control']) }}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('grootboekrekening_nummer', 'Grootboekrekening Nummer') }}
            </div>
            <div class="col-xs-2">
                {{ Form::number('grootboekrekening_nummer',null, array('class' => 'form-control','min' => 0)) }}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('naam', 'Naam Grootboekrekening') }}
            </div>
            <div class="col-xs-10">
                {{ Form::text('naam',null, array('class' => 'form-control')) }}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-2">
                {{ Form::label('type', 'Type') }}
            </div>
            <div class="col-xs-10">
                {{ Form::radio('type', true, false, ['class' => 'radio']) }} Activa  Of Inkomst <br><br>
                {{ Form::radio('type', false, false, ['class' => 'radio']) }} Pasiva  Of Uitgave
            </div>
        </div>

        {{ Form::submit() }}
        {{ Form::close() }}

        <hr>

        <div class="row">
            @include('flash::message')
        </div>
    </div>
@stop
