@extends('layout')
@section('content')
    <div class="container">
        @include('partials.navi-bar')
        <div class="content">
            <div class="hero-unit">
                <div class="title"><h1></h1></div>
                <div class="hero-unit">
                    <div class="row-fluid">
                        <div class="panel-heading"><h1>Instellingen</h1></div>
                        <hr>
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class=" row col-lg-12">
                            <div class="row col-lg-12 well">
                                <div class="row-fluid">
                                    <a href="{{url()->current().'/valuta'}}"><h3><span class="glyphicon glyphicon-arrow-right"></span>
                                            Valutas</h3></a>
                                </div>
                            </div>
                            <hr>
                            <div class="row col-lg-12 well">
                                <div class="row-fluid">
                                    <a href="{{url()->current().'/rubriek'}}"><h3><span class="glyphicon glyphicon-arrow-right"></span>
                                            Rubieken</h3></a>
                                </div>
                                <div class="row-fluid">
                                    <a href="{{url()->current().'/subrubriek'}}"><h3><span class="glyphicon glyphicon-arrow-right"></span>
                                            Subrubieken</h3></a>
                                </div>
                                <div class="row-fluid">
                                    <a href="{{url()->current().'/grootboekrekeningen'}}"><h3><span
                                                    class="glyphicon glyphicon-arrow-right"></span> Grootboekrekeningen</h3></a>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop
