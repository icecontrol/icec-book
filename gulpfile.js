var elixir = require('laravel-elixir');

elixir(function(mix) {
    var datepicker = 'node_modules/eonasdan-bootstrap-datetimepicker/build';
    var bootstrap = 'node_modules/bootstrap/dist';
    var jqueryPath = 'node_modules/jquery';
    var Charts = 'node_modules/chart.js';
    
    mix.sass('app.scss')
        .copy(jqueryPath + '/dist/jquery.min.js', 'public/js')
        .copy(bootstrap + '/js/bootstrap.min.js', 'public/js')
        
        .copy(datepicker + '/css/bootstrap-datetimepicker.min.css', 'public/css')
        .copy(datepicker + '/js/bootstrap-datetimepicker.min.js', 'public/js')
        
        .copy(bootstrap + '/css/bootstrap.min.css', 'public/css')
        .copy(bootstrap + '/css/bootstrap.min.css.map', 'public/css')
        .copy('node_modules/font-awesome/css/font-awesome.min.css', 'public/fonts')

        .copy(Charts + '/dist/Chart.bundle.js', 'public/js')
        .copy(Charts + '/dist/Chart.bundle.min.js', 'public/js');
    
});